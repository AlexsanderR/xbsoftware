package XBSoftware_Tests;


import XBSoftware_Basic.SpecMethodAndVariables;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class PrivacyPolicyTest extends SpecMethodAndVariables{

    @Test
    public void linkXBsoftware(){
        driver.get("http://xbsoftware.com/privacy-policy/");
        findElementClick(By.cssSelector("a[href='http://xbsoftware.com/']"));
        assertElements(By.cssSelector("span:contains('What We Do')"));
    }

    @Test
    public void contactUs(){
        driver.get("http://xbsoftware.com/privacy-policy/");
        findElementClick(By.linkText("contact us"));
        assertText("Contact us");
    }

    @Test
    public void informationTest(){
        driver.get("http://xbsoftware.com/privacy-policy/");
        assertElements(info);
    }

    @Test
    public void upTest(){
        driver.get("http://xbsoftware.com/privacy-policy/");
        findElementClick(buttonUp);
    }


}
