package XBSoftware_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class PortfolioCaseStudiesTest extends XBSoftware_Basic.SpecMethodAndVariables{

	By linkCaseStudies = By.xpath("//*[@id='menu-item-414']/a");
	                                       
			
	 
	@Test
	public void linkCaseStudies(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		assertText("Case Studies");
	}
	
	@Test
	public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(goToMainPage);
		assertText("What We Do");
	}

	@Test 
	public void linkXBSoftware (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(By.xpath("//a[@title='XB Software company']"));
		assertText("What We Do");
	}
	
	@Test
	public void linkProjectsOverview (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(By.xpath("//a[@href='/projects-overview/']"));
		assertText("Projects Overview");
	}
	
	@Test
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(buttonUp);
	}
	
	@Test
	public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		assertElements(info);
	}
	

	@Test
	public void buttonViewCaseOnlineShoppingService(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
	    findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/online-shopping-service/']"));
        assertElements(By.xpath("//*[.='Online Shopping Service']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));
	}

    @Test
    public void buttonViewCaseWorkflowApplicationForBusinesses(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/workflow-app-for-business/']"));
        assertElements(By.xpath("//*[.='Workflow Application For Businesses']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));

    }

	@Test
	public void buttonViewCaseWebBasedEducatorDevelopedSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/educator-developed-software/']"));
        assertElements(By.xpath("//*[.='Web-based Educator Developed Software']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));
	}
	
	@Test
	public void buttonViewCaseProjectManagementAndWorkflowCloudSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/project-management-and-workflow/']"));
        assertElements(By.xpath("//*[.='Project Management and Workflow Cloud Software']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));
	}
	
	@Test
	public void buttonViewCaseSocialPlatform (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
        waitForElement(By.xpath("//*[.='Social Platform']"));
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/social-platform/']"));
        assertElements(By.xpath("//*[.='Social Platform']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));
	}

    @Test
    public void buttonViewCaseEmployeePlanningSystem(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/employee-planning-system/']"));
        assertElements(By.xpath("//*[.='Employee Planning System']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));

    }

    @Test
    public void buttonViewCaseProjectManagementToolGorCommunicationsPortal(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/case-studies/project-management-tool-portal/']"));
        assertElements(By.xpath("//*[.='Project Management Tool For Communications Portal']"));
        findElementClick(By.xpath("//*[.='Download case in PDF']"));

    }
	
	// test for Technologies.  Checked five elements, one for each project.
	
	@Test
	public void portfolioCaseStudiesTechnologiesClickActiveInactive (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(5) > span")); // click on D3.js (inactive) � ������� ������
		assertElements(By.cssSelector("body > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span")); // ����� D3.js � ������� Workflow Application For Businesses (active)
		
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span")); // click on D3.js (active) � ������� ������
		assertElements(By.cssSelector("body > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a:nth-child(2) > span")); // ����� D3.js � ������� Workflow Application For Businesses (inactive)
		
		
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(21) > span"));  //click YII FRAMEWORK
		assertElements(By.cssSelector("body > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span")); // �� �� ��������, ������� � ������� ������, ��������� � ������.
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a:nth-child(4) > span"));
		
		
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(15) > span")); //SCRUM
		assertElements(By.cssSelector("body > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a.active > span"));
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > section.section-products > div > div:nth-child(5) > div.col-sm-7.tech_post > div > a:nth-child(4) > span"));
		
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(1) > span"));//Amazon S3
		assertElements(By.cssSelector("body > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span"));
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > section.section-products > div > div:nth-child(7) > div.col-sm-7.tech_post > div > a:nth-child(1) > span"));
		
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(14) > span"));//ruby on rails
		assertElements(By.cssSelector("body > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span"));
		findElementClick(By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > section.section-products > div > div:nth-child(9) > div.col-sm-7.tech_post > div > a:nth-child(9) > span"));
		
		
	}

    @Test
    public void readMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-414')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@class='btn btn-send']"));
        findElementClick(By.xpath("//*[@class='text-link-to-pdf border-radius-10']"));

    }
}

