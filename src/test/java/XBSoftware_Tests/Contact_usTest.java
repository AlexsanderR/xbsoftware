package XBSoftware_Tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class Contact_usTest extends XBSoftware_Basic.SpecMethodAndVariables{
		By ContactUs = By.xpath("//*[@id='menu-item-22']/a");
		By ZoomOut = By.xpath("//div[@title='Zoom out']");
		By ZoomIn = By.xpath("//div[@title='Zoom in']");
		By goToContactUs = By.xpath("//a[contains(text(),'Contact us')]");
		By SendMessage = By.xpath("//input[@type='submit'][@value='Send']");
	    //findElementClick(By.xpath("//input[@type='submit'][@value='Send']"));
	@Test 
		public void ContactUs(){
			findElementClick(goToContactUs);
			findElementClick(ContactUs);
			findElementClick(ContactUs);
			findElementClick(ZoomOut);
			findElementClick(ZoomOut);
			findElementClick(ZoomIn);
			findElementClick(ZoomIn);
		}
	
	@Test 
		public void ContactUsSendMessage(){
			findElementClick(goToContactUs);
			findElementClick(SendMessage);
			assertElements(By.xpath("//label[contains(text(),'This field is required.')]"));
			inputField(By.xpath("//input[@type='text'][@name='user_name']"), "test");
			inputField(By.xpath("//input[@type='email'][@name='user_email']"), "test");
			findElementClick(SendMessage);
			assertElements(By.xpath("//label[contains(text(),'Please enter a valid email address.')]"));
			inputField(By.xpath("//input[@type='email'][@name='user_email']"), "test@test.qa");
			assertElements(By.xpath("//label[contains(text(),'This field is required.')]"));
			inputField(By.xpath("//textarea[@name='user_message'][@id='inputMessage']"), "b9pv1j2yzm1jsqritazk");
			findElementClick(SendMessage);
            timeout();
			//waitForElement(By.cssSelector("#wpcf7-f43-p11-o1 > form > div.wpcf7-response-output.wpcf7-display-none.wpcf7-mail-sent-ok"));
			assertText("Validation errors occurred. Please confirm the fields and submit it again.");

			
}

}
