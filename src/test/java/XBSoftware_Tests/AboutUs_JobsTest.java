package XBSoftware_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class AboutUs_JobsTest extends XBSoftware_Basic.SpecMethodAndVariables {

	@Test
	public void linkAboutUsJobs (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-930')[0].parentNode.style.display = 'block'");
		findElementClick(By.xpath("//*[@id='menu-item-930']/a"));
		assertText("О нас"); 
	}
	
}
