package XBSoftware_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ServicesTest extends XBSoftware_Basic.SpecMethodAndVariables {

    @Test
    public void linkServices() {
        findElementClick(linkServices);
        assertText(textServicePage);
    }

    @Test
    public void linkXBSoftware() {
        findElementClick(linkServices);
        findElementClick(linkXB_Software);
        assertText(whatWeDo);
    }

    @Test
    public void linkReadMore() {
        findElementClick(linkServices);
        findElementClick(linkReadMore);
        assertText(technologyExpertise);
    }

    @Test
    public void arrowUp() {
        findElementClick(linkServices);
        findElementClick(buttonUp);
    }

    @Test
    public void goToMainPage() {
        findElementClick(linkServices);
        findElementClick(goToMainPage);
        assertText(whatWeDo);
    }

    @Test
    public void emailAddress() {
        findElementClick(linkServices);
        assertElements(info);
    }

    By servicesSend = By.xpath("//input[@value='Send']");

    @Test
    public void servicesSendMessage() {
        findElementClick(linkServices);
        findElementClick(servicesSend);
        assertText("This field is required.");
        inputField(inputName, inputNameText);
        inputField(inputEmail, sendIncorrectEmail);
        assertText("Please enter a valid email address.");
        inputField(inputEmail, inputEmailText);
        inputField(inputPhone, sendIncorrectPhone);
        assertText("Please enter a valid phone number.");
        inputField(inputPhone, inputPhoneNumber);
        assertText("This field is required.");
        inputField(inputMessage, inputMessageText);
        findElementClick(servicesSend);
        timeout();
        assertText("Validation errors occurred. Please confirm the fields and submit it again.");
    }

    @Test
    public void servicesWebDevelopment() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-640')[0].parentNode.style.display = 'block'");
        findElementClick(servicesWebDevelopment);
        assertText(textServicesWebDevelopment);
    }

    @Test
    public void servicesBusinessAnalysis() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-641')[0].parentNode.style.display = 'block'");
        findElementClick(servicesBusinessAnalysis);
        assertText(textServicesBusinessAnalysis);
    }

    @Test
    public void servicesQualityAssurance() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-642')[0].parentNode.style.display = 'block'");
        findElementClick(servicesQualityAssurance);
        assertText(textServicesQualityAssurance);
    }

    @Test
    public void servicesInternetMarketing() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1556')[0].parentNode.style.display = 'block'");
        findElementClick(By.xpath("//*[@id='menu-item-1556']/a"));
        assertText("Internet Marketing Services");
    }

    @Test
    public void servicesInternetMarketingSEO() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-24 ul')[0].style.display = 'block'");
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1556 ul')[0].style.display = \"block\";");
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"li[id='menu-item-1557'] a\").click()");
        assertText("Search Engine Optimization (SEO)");

    }

    @Test
    public void servicesInternetMarketingSMM(){
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-24 ul')[0].style.display = 'block'");
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1556 ul')[0].style.display = \"block\";");
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"li[id='menu-item-1558'] a\").click()");
        assertText("Social Media Marketing (SMM)");
    }

    @Test
    public void servicesInternetMarketingSMO(){
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-24 ul')[0].style.display = 'block'");
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1556 ul')[0].style.display = \"block\";");
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"li[id='menu-item-1559'] a\").click()");
        assertText("Social Media Optimization (SMO)");
    }

    @Test
    public void servicesReadMoreAudit(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/website-seo-audit/']"));
        assertText("Full Site SEO Audit");
    }

    @Test
    public void servicesReadMoreSEO(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/search-engine-optimization/']"));
        assertText("Search Engine Optimization (SEO)");
    }

    @Test
    public void servicesReadMoreCRO(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/conversion-rate-optimization/']"));
        assertText("Conversion Rate Optimization (CRO)");
    }

    @Test
    public void servicesReadMorePPC(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/pay-per-click-advertising-management/']"));
        assertText("Pay-Per-Click Advertising Management (PPC)");
    }

    @Test
    public void servicesReadMoreContentMarketing(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/content-marketing/']"));
        assertText("Content Marketing");
    }

    @Test
    public void servicesReadMoreWebAnalystics(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/web-analytics/']"));
        assertText("Coherent Web-Analytics");
    }

    By request = By.xpath("//input[@value='Request Web-Analytics For Your Site']");
    @Test
    public void requestWEbAnalistics(){
        findElementClick(linkServices);
        findElementClick(By.xpath("//a[@href='/services/web-analytics/']"));
        assertText("Coherent Web-Analytics");
        findElementClick(request);
        assertText("This field is required.");
        inputField(By.id("inputName"), "test");
        assertText("This field is required.");

        inputField(By.id("inputEmail"), "test");
        assertText("Please enter a valid email address.");
        inputField(By.id("inputEmail"), "test@test.test");
        assertText("This field is required.");

        inputField(By.id("inputSite"), "test");
        assertText("Please enter a valid URL.");
        inputField(By.id("inputSite"), "http://xbsoftware.com/");
        assertText("This field is required.");

        inputField(By.id("inputMessage"), "test");
        findElementClick(request);
        timeout();
        assertText("Validation errors occurred. Please confirm the fields and submit it again.");
    }


}
