package XBSoftware_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsXBtrackTest  extends XBSoftware_Basic.SpecMethodAndVariables {
	
	//
	
	By linkXBtrack = By.xpath("//*[@id='menu-item-860']/a");
	
	
	@Test 
	public void linkProductsXBtrack(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		assertText("This products is distributed as a SaaS solution");
	}
	
	@Test
	public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		findElementClick(goToMainPage);
		assertText("What We Do");
	}
	
	@Test
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		findElementClick(buttonUp);
	}
	
	@Test
	public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		assertElements(info);
	}
	
	@Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
		assertText("What We Do");
	}
	
	@Test
	public void buttonGetYourFreeQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.linkText("Get Your Free Quote"));
		goToURL(currentWindows, "http://xbsoftware.com/request-a-quote/");
		assertText("Request a quote");
	}
	
	@Test
	public void buttonViewDemo(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-860')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.linkText("View Demo"));
		goToURL(currentWindows, "http://xbsoftware.com/demos/btrack/login.php");
		//assertText("XBtrack - Task and Bug Tracking System");
	}	
	

}
