package XBSoftware_Tests;



import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class MainPageTest extends XBSoftware_Basic.SpecMethodAndVariables{
	
		
	@Test
	public void whatWeDoTest()  {
		findElementClick(imageWhatWeDo);
		assertText(readMoreAssert);
	}
	
	@Test
	public void readMoreTest() {
		findElementClick(ReadMore);   
		assertText(readMoreAssert);
	}
	
	
	@Test
	public void carouselTest(){
		findElementClick(carouselLeft);
		assertText(carouselLastSlideAssert);
		timeout();
		for (int i=0; i<3; i++){
		findElementClick(carouselRight);
		timeout();}
		findElementClick(carouselSlide);
		assertText(carouselFirstSlideAssert);		
	}
	
	@Test 
	public void linkGoWebixTest(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(linkGoToWebix);
		goToURL(currentWindows, linkWebix);
	
	}

    @Test
    public void goToStaffManager(){
        findElementClick(By.cssSelector("a[title='XB Staff Manager']"));
        assertElements(By.xpath("//*[.='Customizable Employee Management System']"));
    }

    @Test
    public void goToBTrack(){
        findElementClick(By.cssSelector("a[title='XBtrack']"));
        assertElements(By.xpath("//*[.='Flexible Task and Bug Tracking System']"));
    }

    @Test
    public void goToEnjoyHint(){
        findElementClick(By.cssSelector("a[title='http://xbsoftware.com/products/enjoyhint/']"));
        assertElements(By.xpath("//*[.='Interactive hints and tips for your website or app']"));
    }

    @Test
    public void ourClient(){
        findElementClick(By.cssSelector("a[title='read testimonials']"));
        assertElements(By.xpath("//*[.='Testimonials']"));
    }
	
	@Test
	public void upTest(){
		findElementClick(buttonUp);
				
	}
	
	@Test
	public void testimonialsTest(){
		findElementClick(testimonialsLink);
		
		assertText(testimonialsAssert);
	}
	
	@Test
	public void informationTest(){
		assertElements(info);
		
	}
	
	@Test
	public void goToRUTest(){
		findElementClick(By.linkText("ru"));
		assertText("Наши услуги");
	}

    @Test
    public void text(){
        assertElements(By.xpath("//*[.='Our News']"));
        assertElements(By.xpath("//*[.='IT Events - Let′s Meet']"));
        assertElements(By.xpath("//*[.='Industries']"));
        assertElements(By.xpath("//*[.='Twitter']"));
    }

    @Test
    public void privacyPolicy(){
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.linkText("Privacy policy"));
        assertElements(By.cssSelector("span:contains('Privacy Policy')"));
    }
	


}

