package XBSoftware_Tests;

import XBSoftware_Basic.SpecMethodAndVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class BlogTest extends SpecMethodAndVariables{
	
	By blog = By.xpath("//*[@id='menu-item-118']/a");
	
	@Test
	public void TagsAllPostsAndOtgersTags(){
		findElementClick(blog);
		findElementClick(By.className("post_title"));
		findElementClick(By.xpath("//span[@class='truncate'][contains(text(), 'All posts')]"));
		assertText("Sign up to our newsletter and get the latest news and updates");
		
	}
		
	
	@Test
	public void RecentPost (){
		findElementClick(blog);
        ((JavascriptExecutor)driver).executeScript("document.querySelector(\".recent-posts-links li:nth-child(1) a\").click()");
		assertElements(By.xpath("//*[.='Recent Posts']"));
        timeout();
	}
	
	@Test
	public void ReadMore (){
		findElementClick(blog);
		findElementClick(By.linkText("Read more"));
		findElementClick(By.linkText("Blog"));
		assertText("Recent Posts");
	}
	
	@Test
	public void emailAddress(){
		findElementClick(blog);
	assertElements(info);
	}
	
	@Test
	public void goHomePage(){
		findElementClick(blog);
		findElementClick(goToMainPage);
		assertText("What We Do");
	}
	
	@Test
	public void linkXBSoftware (){
		findElementClick(blog);
		findElementClick(By.xpath("//a[@href='http://xbsoftware.com/']"));
		assertText(whatWeDo);
	}
	

	@Test
	public void linkOnFirstPostOnPage (){
		findElementClick(blog);
		findElementClick(By.className("post_title"));
		findElementClick(By.linkText("Blog"));
		
	}
	
	@Test
	public void linkLeaveAComment(){
		findElementClick(blog);
		findElementClick(By.xpath("//div[@class='comments-count']")); 
		assertText("Comments");
	}
	
	
	
	@Test
	public void pagination(){
		findElementClick(blog);
		findElementClick(By.linkText("2")); 
		findElementClick(By.xpath("//a[@href='http://xbsoftware.com/blog/']"));
	}
	
	@Test
	public void arrowUp(){
		findElementClick(blog);
		findElementClick(buttonUp);
	}
	
	/*@Test
	public void commentForm(){
		findElementClick(blog);
		findElementClick(By.xpath("//div[@class='comments-count']")); 
		findElementClick(By.xpath("//input[@id='submit']")); //click on button Submit Comment
		assertElements(By.xpath("//label[contains(text(),'This field is required.')]"));
		inputField(By.id("author"), "Test"); //full name
		inputField(By.id("email"), "Test"); //Email
		assertElements(By.xpath("//label[contains(text(), 'Please enter a valid email address.')]"));
		inputField(By.id("email"), "Test@test.test"); //Email
	    assertElements(By.xpath("//*[.='This field is required.']"));
		inputField(By.id("comment"), "Test"); //comment
		//click on button Submit Comment
		findElementClick(By.id("notify")); //check-box
		findElementClick(By.xpath("//input[@id='submit']"));

	}*/

	By singUp = By.cssSelector(".subscribe-block-btn");
    By name = By.xpath("//input[@name='subscription_name']");
    By email = By.xpath("//input[@name='subscription_email']");

	@Test
	public void singUp(){
		findElementClick(blog);
		findElementClick(singUp);
        assertElements(By.cssSelector("label:contains('Please enter a name')"));
		inputField(name, "test");
        assertElements(By.cssSelector("label:contains('Please enter an email')"));
		inputField(email, "test");
        assertElements(By.cssSelector("label:contains('Please enter an email')"));
		inputField(email, "тест@тест.тест");
        findElementClick(singUp);
		timeout();
        assertElements(By.cssSelector("label:contains('Please enter an email')"));
		inputField(email, "test@test.test");
        findElementClick(singUp);
	    timeout();
        assertElements(By.xpath("//*[.='You are already subscribed']"));
		findElementClick(blog);
		inputField(name, "test");
		String emailAddress = "test" + randomText(5) + "@test.test" ;
		inputField(email, emailAddress);
        findElementClick(singUp);
		timeout();
        assertElements(By.xpath("//*[.='Thank you for subscribing']"));
	}

    @Test
    public void privacyPolicy(){
        findElementClick(blog);
        findElementClick(By.cssSelector(".row-subscr-form a"));
        assertElements(By.cssSelector("span:contains('Privacy Policy')"));
    }

}

	
	
	