package XBSoftware_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class AboutUs_TechnologyExpertiseTest extends XBSoftware_Basic.SpecMethodAndVariables{

   	@Test
	public void linkTechnologyExpertise(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-639')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		assertText(technologyExpertise);
	}

	@Test
	public void goHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-639')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		findElementClick(goToMainPage);
		assertText(whatWeDo);
	}

	@Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-639')[0].parentNode.style.display = 'block'");;
		findElementClick(linkTechnologyExpertise);
		findElementClick(linkXBSoftware);
		assertText(whatWeDo);
		
	}
	
	@Test 
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-639')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		findElementClick(buttonUp);
	}
	
	@Test 
	public void requestAQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-639')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		findElementClick(requestAQuote);
		assertText(requestaQuote);
	}
}
