package XBSoftware_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class AboutUs_TestimonialsTest extends XBSoftware_Basic.SpecMethodAndVariables{
	
	@Test
	public void linkTestimonials(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-638')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		waitForElement(By.id("arrow-up"));
		assertText(testimonials);
	}

	@Test
	public void softwareDevelopmentCompany(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-638')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		waitForElement(softwareDevelopmentCompany);
		findElementClick(softwareDevelopmentCompany);
		waitForElement(By.id("arrow-up"));
		assertText(whatWeDo);
	}
	
	@Test 
	public void requestAQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-638')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		findElementClick(requestAQuote);
		waitForElement(By.id("arrow-up"));
		assertText(requestaQuote);
	}
	
	@Test 
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-638')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		waitForElement(buttonUp);
		findElementClick(buttonUp);
	}
	
	@Test
	public void goHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-638')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		waitForElement(goToMainPage);
		findElementClick(goToMainPage);
		waitForElement(By.id("arrow-up"));
		assertText(whatWeDo);
	}
	
	@Test
	public void information(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-638')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		waitForElement(info);
		waitForElement(By.id("arrow-up"));
		assertElements(info);
		
	}
}

