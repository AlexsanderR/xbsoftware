package XBSoftware_Tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class RequestAQuoteTest extends XBSoftware_Basic.SpecMethodAndVariables{
	
	By linkRequestAQuote = By.xpath("/html/body/header/div[1]/div/a");
	By buttonSend = By.xpath("//input[@type='submit'][@value='Send']");
	By inputName = By.id("inputName");
	By inputEmail = By.id("inputEmail");
	By inputPhone = By.id("inputPhone");
	By attachFile = By.name("userfile_1");
	

	@Test
	public void openRequestAQuote(){
		findElementClick(linkRequestAQuote);
		assertText("Request a quote");
	}
	
	@Test
	public void attachFile(){
		findElementClick(linkRequestAQuote);
		uploadedFile(attachFile, "src/test/filesForUplod/412.png");
		timeout();
		assertText("412.png");
		timeout();
		findElementClick(By.xpath("//span[@class='icon icon-delete']"));		
	}
	
	@Test
	public void sendMessage(){
		findElementClick(linkRequestAQuote);
		findElementClick(buttonSend);
		assertText("This field is required.");
		
		inputField(inputName, "Test");
		assertText("This field is required.");
		findElementClick(buttonSend);
		
		inputField(inputEmail, "test");
		assertText("Please enter a valid email address.");
		
		inputField(inputEmail, "test@test.test");
		assertText("This field is required.");
		findElementClick(buttonSend);
		
		inputField(inputPhone, "test");
		assertText("Please enter a valid phone number.");
		
		inputField(inputPhone, "1111111");
		assertText("This field is required.");
		findElementClick(buttonSend);
		
		inputField(By.id("inputCompany"), "company");
		assertText("This field is required.");
		findElementClick(buttonSend);
		
		inputField(By.id("inputMessage"), "b9pv1j2yzm1jsqritazk");
		
		uncheckedElements(By.xpath("//input[@type='checkbox'][@value='Send me an NDA']"));
		findElementClick(By.xpath("//input[@type='checkbox'][@value='Send me an NDA']"));
		uploadedFile(attachFile, "src/test/filesForUplod/412.png");
		timeout();
		findElementClick(buttonSend);
		timeout();
	    assertElements(By.xpath("//*[.='Validation errors occurred. Please confirm the fields and submit it again.']"));

	}
	
	@Test
	public void up(){
		findElementClick(linkRequestAQuote);
		findElementClick(buttonUp);
				
	}
}