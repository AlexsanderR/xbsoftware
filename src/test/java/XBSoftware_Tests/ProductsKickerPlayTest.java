package XBSoftware_Tests;

import XBSoftware_Basic.SpecMethodAndVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class ProductsKickerPlayTest extends SpecMethodAndVariables{

    By linkKickerPlay = By.xpath("//*[@id='menu-item-2251']/a");


    @Test
    public void linkProductsKickerPlay(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        assertText("Simple App to Manage Kicker Tournaments Online");
    }

    @Test
    public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        findElementClick(goToMainPage);
        assertText("What We Do");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        findElementClick(buttonUp);
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        waitForElement(By.id("arrow-up"));
        assertElements(info);
    }


    @Test
    public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
        waitForElement(By.id("arrow-up"));
        assertText("What We Do");
    }


    @Test
    public void otherProducts(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        assertText("Other Products:");
        findElementClick(By.xpath("//*[@class='caption-product-link']"));
    }

    @Test
    public void getYourFreeAccount(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.cssSelector(".btn-send"));
        goToURL(currentWindows, "http://kickerplay.com");
    }

    @Test
    public void carusel(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        findElementClick(By.cssSelector(".roundabout-holder a"));
        //timeout();
        findElementClick(By.cssSelector(".mfp-arrow-right"));
        assertElements(By.cssSelector(".mfp-counter:contains('2 of 8')"));
        findElementClick(By.cssSelector(".mfp-arrow-left"));
        assertElements(By.cssSelector(".mfp-counter:contains('1 of 8')"));
        findElementClick(By.cssSelector(".mfp-close"));
        assertText("Simple App to Manage Kicker Tournaments Online");
    }

    @Test
    public void readMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2251')[0].parentNode.style.display = 'block'");
        findElementClick(linkKickerPlay);
        findElementClick(By.cssSelector(".list-features-project a"));
        assertText("Meet KickerPlay: Online Kicker Manager App");
    }

}
