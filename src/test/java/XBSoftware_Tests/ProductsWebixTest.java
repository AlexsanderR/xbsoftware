package XBSoftware_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class ProductsWebixTest extends XBSoftware_Basic.SpecMethodAndVariables {
	
	// variables
	
		By linkWebix = By.xpath("//*[@id='menu-item-859']/a");
				
		
		@Test 
		public void linkProductsWebix(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			assertText("Fast JavaScript UI Widgets Library");
		}
		
		@Test
		public void goHomePage (){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			findElementClick(goToMainPage);
			assertText("What We Do");
		}
		
		@Test
		public void arrowUp(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			findElementClick(buttonUp);
		}
		
		@Test
		public void emailAddress(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			waitForElement(By.id("arrow-up"));
			assertElements(info);
		}
		
		
		@Test
		public void linkXBSoftware(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
			waitForElement(By.id("arrow-up"));
			assertText("What We Do");
		}
		
		@Test
		public void buttonReadMore(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			findElementClick(By.linkText("Read more"));
			assertText("Plenty of UI Widgets");
		}
		
		@Test
		public void buttonViewDemo(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
			findElementClick(linkWebix);
			String currentWindows = driver.getWindowHandle();
			findElementClick(By.xpath("//a[@title='View Demo']"));
			goToURL(currentWindows, "http://webix.com/all_demos/");
			
		}

        @Test
        public void otherProducts(){
            ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-859')[0].parentNode.style.display = 'block'");
            findElementClick(linkWebix);
            assertText("Other Products:");
            findElementClick(By.xpath("//*[@class='caption-product-link']"));
        }

}
