package XBSoftware_Tests;

import XBSoftware_Basic.SpecMethodAndVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class ProductsWebixCRMTest extends SpecMethodAndVariables{

    By linkWebixCRM = By.xpath("//*[@id='menu-item-2061']/a");


    @Test
    public void linkProductsWebixCRM(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        assertText("Handy Online CRM System For Successful Customer Management");
    }

    @Test
    public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        findElementClick(goToMainPage);
        assertText("What We Do");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        findElementClick(buttonUp);
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        waitForElement(By.id("arrow-up"));
        assertElements(info);
    }


    @Test
    public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
        waitForElement(By.id("arrow-up"));
        assertText("What We Do");
    }


    @Test
    public void otherProducts(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        assertText("Other Products:");
        findElementClick(By.xpath("//*[@class='caption-product-link']"));
    }

    @Test
    public void getYourFreeQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.cssSelector("a[href='/request-a-quote/']"));
        goToURL(currentWindows, "http://xbsoftware.com/request-a-quote/");
    }

    @Test
    public void carusel(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        findElementClick(By.cssSelector(".roundabout-holder a"));
        //timeout();
        findElementClick(By.cssSelector(".mfp-arrow-right"));
        assertElements(By.cssSelector(".mfp-counter:contains('2 of 8')"));
        findElementClick(By.cssSelector(".mfp-arrow-left"));
        assertElements(By.cssSelector(".mfp-counter:contains('1 of 8')"));
        findElementClick(By.cssSelector(".mfp-close"));
        assertText("Handy Online CRM System For Successful Customer Management");
    }

    @Test
    public void readMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-2061')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebixCRM);
        findElementClick(By.cssSelector(".list-features-project a"));
        assertText("Handy Webix CRM For Successful Customer Management");
    }


}
