package XBSoftware_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

import XBSoftware_Basic.SpecMethodAndVariables;

public class SearchTest extends SpecMethodAndVariables{
	
	By search = By.className("search-icon");
	By inputSearch = By.xpath("//input[@id='search']");
	By searchElement = By.xpath("//input[@class='pull-right']");


    @Test
    public void hideSearch(){
        findElementClick(search);
        findElementClick(By.className("hide-search-form"));
    }
	@Test
	public void searchEmptyInformation(){
		findElementClick(search);
		findElementClick(searchElement);
		assertText("results");

	}

    @Test
    public void searchWithError(){
        findElementClick(search);
        inputField(inputSearch, "testtesttest");
        findElementClick(searchElement);
        waitForElement(By.cssSelector(".title-404"));
        assertText("Sorry, no posts matched your search criteria.");
        //findElementClick(search);

    }

    @Test void search(){
        findElementClick(search);
        inputField(inputSearch, "test");
        findElementClick(searchElement);
        timeout();
        assertText("results");
    }

    @Test
    public void goToNextPage(){
        findElementClick(search);
        findElementClick(searchElement);
        waitForElement(By.linkText("3"));
        findElementClick(By.linkText("3"));
    }

    @Test
    public void goToNextPost(){
        findElementClick(search);
        findElementClick(searchElement);
        waitForElement(By.linkText("Next post"));
        findElementClick(By.linkText("Next post"));
    }

    @Test
    public void homeLink(){
        findElementClick(search);
        inputField(inputSearch, "testtesttest");
        findElementClick(searchElement);
        waitForElement(By.cssSelector(".home-link"));
        findElementClick(By.cssSelector(".home-link"));
    }


}
