package XBSoftware_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class PortfolioTest extends XBSoftware_Basic.SpecMethodAndVariables{

	
	
	
	@Test 
	public void linkPortfolio(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-644')[0].parentNode.style.display = 'block'");
		findElementClick(linkPortfolio);
		assertText("Projects Overview");
		
	}
	
	@Test
	public void linksInProjectsOverview(){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[@href='#business']")); 
		assertURL("http://xbsoftware.com/projects-overview/#business");
		findElementClick(By.xpath("//a[@href='#marketing']"));
		assertURL("http://xbsoftware.com/projects-overview/#marketing");
		findElementClick(By.xpath("//a[@href='#financial']"));
		assertURL("http://xbsoftware.com/projects-overview/#financial");
		findElementClick(By.xpath("//a[@href='#information']"));
		assertURL("http://xbsoftware.com/projects-overview/#information");
		findElementClick(By.xpath("//a[@href='#logistics']"));
		assertURL("http://xbsoftware.com/projects-overview/#logistics");
		findElementClick(By.xpath("//a[@href='#education']"));
		assertURL("http://xbsoftware.com/projects-overview/#education");
		findElementClick(By.xpath("//a[@href='#customer']"));
		assertURL("http://xbsoftware.com/projects-overview/#customer");
		findElementClick(By.xpath("//a[@href='#e-commerce']"));
		assertURL("http://xbsoftware.com/projects-overview/#e-commerce");
	}
	
	@Test
	public void linkCaseStudies (){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[@href='/case-studies/']"));
		assertText("Case Studies");
	}
	
	@Test 
	public void linkSoftwareDevelopmentCompany(){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[contains(text(),'software development company')]"));
		assertText("What We Do");
		}
	
	@Test
	public void requestAQuote (){
		
	findElementClick(linkPortfolio);
	findElementClick(By.className("to-request-btn"));
	assertText("Request a quote");
	}
		
	@Test
	public void arrowUp(){
		findElementClick(linkPortfolio);
		findElementClick(buttonUp);
	}
	
	@Test
	public void emailAddress (){
		findElementClick(linkPortfolio);
		assertElements(info);
	}
	
	@Test
	public void goHomePage(){
		findElementClick(linkPortfolio);
		findElementClick(goToMainPage);
		assertText("What We Do");
	}
	
	@Test
	public void linkProjectOverview(){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[@href='http://xbsoftware.com/projects-overview/']"));
		assertText("Projects Overview");
	}


}
