package XBSoftware_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;


public class AboutUsTest extends XBSoftware_Basic.SpecMethodAndVariables{
	
	 
	By send = By.xpath("//input[@value='Send']");
	 
	  
	@Test 
	public void linkSoftwareDevelopmentCompany(){
		findElementClick(linkAboutUs);
		findElementClick(By.linkText("software development company"));
		assertText("We Use");
	}
	
	@Test
	public void linkBusinesAnalysis(){
		findElementClick(linkAboutUs);
		findElementClick(By.linkText("business analysis"));
		assertText("Business Analysis");
		
	}
	//
	@Test
	public void linkWebDevelopment(){
		findElementClick(linkAboutUs);
		findElementClick(By.linkText("web development"));
		assertText("Web Development");
	}
	
	@Test
	public void linkQualityAssurance(){
		findElementClick(linkAboutUs);
		findElementClick(By.linkText("quality assurance"));
		assertText("Quality Assurance");
	}
	
	@Test
	public void sendMessage(){
		findElementClick(linkAboutUs);
		findElementClick(send);
		assertText("This field is required.");
		inputField(By.id("inputName"), "Test");
		inputField(By.id("inputEmail"), "test");
		assertText("Please enter a valid email address.");
		inputField(By.id("inputEmail"), "test@test.test");
		inputField(By.id("inputPhone"), "test");
		assertText("Please enter a valid phone number.");
		inputField(By.id("inputPhone"), "111111111");
		assertText("This field is required.");
		inputField(By.id("inputMessage"), "b9pv1j2yzm1jsqritazk");
		findElementClick(send);
        timeout();
        assertText("Validation errors occurred. Please confirm the fields and submit it again.");

	}
	
	@Test
	public void up(){
		findElementClick(linkAboutUs);
		findElementClick(buttonUp);
				
	}
	
	@Test
	public void goToMainPage(){
		findElementClick(linkAboutUs);
		findElementClick(By.xpath("/html/body/header/div[2]/div[2]/div/a/span"));
		assertText("We Use");
		
	}

    @Test
    public void more(){
        findElementClick(linkAboutUs);
        findElementClick(By.xpath("//a[.='more']"));
        assertElements(By.xpath("//*[.='Projects Overview']"));
        timeout();

    }
}
