package XBSoftware_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsXBStaffManagerTest extends XBSoftware_Basic.SpecMethodAndVariables {

	// variables
	
	By linkXBStaffManager = By.xpath("//*[@id='menu-item-858']/a");
	
	
	@Test 
	public void linkProductsXBStaffManager(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		assertText("Customizable Employee Management System");
	}
	
	@Test
	public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		findElementClick(goToMainPage);
		assertText("What We Do");
	}
	
	@Test
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		findElementClick(buttonUp);
	}
	
	@Test
	public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		assertElements(info);
	}
	
	
	@Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
		assertText("What We Do");
	}
	
	@Test
	public void buttonGetYourFreeQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.linkText("Get Your Free Quote"));
		goToURL(currentWindows, "http://xbsoftware.com/request-a-quote/");
		assertText("Request a quote");
	}

    @Test
    public void carusel(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        findElementClick(By.cssSelector(".roundabout-holder a"));
        //timeout();
        findElementClick(By.cssSelector(".mfp-arrow-right"));
        assertElements(By.cssSelector(".mfp-counter:contains('2 of 3')"));
        findElementClick(By.cssSelector(".mfp-arrow-left"));
        assertElements(By.cssSelector(".mfp-counter:contains('1 of 3')"));
        findElementClick(By.cssSelector(".mfp-close"));
        assertText("Customizable Employee Management System");
    }

    @Test
    public void findOutMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-858')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        findElementClick(By.linkText("Find out more"));
        assertText("The Staff and Resource Management System You Need");
    }
	
}
