package XBSoftware_Tests;

import XBSoftware_Basic.SpecMethodAndVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class ProductsEnjoyHintTest extends SpecMethodAndVariables {

    By linkEnjoyHint = By.xpath("//*[@id='menu-item-1437']/a");


    @Test
    public void linkProductsEnjoyHint(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        assertText("Interactive hints and tips for your website or app");
    }

    @Test
    public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        findElementClick(goToMainPage);
        assertText("What We Do");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        findElementClick(buttonUp);
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        assertElements(info);
    }


    @Test
    public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
        assertText("What We Do");
    }

    @Test
    public void buttonGetYourFreeEnjoyHint(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.linkText("Get FREE EnjoyHint"));

    }

    @Test
    public void buttonViewDemo(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1437')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[@title='View Demo']"));
        goToURL(currentWindows, "http://xbsoftware.com/demos/enjoyhint/");
     }
}
