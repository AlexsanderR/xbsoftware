package XBSoftware_Tests;


import XBSoftware_Basic.SpecMethodAndVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class ProductsEnjoyCSSTest extends SpecMethodAndVariables{

    By linkEnjoyCSS = By.xpath("//*[@id='menu-item-1728']/a");


    @Test
    public void linkProductsEnjoyHint(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        assertText("Free feature-rich generator of CSS code");
    }

    @Test
    public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        findElementClick(goToMainPage);
        assertText("What We Do");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        findElementClick(buttonUp);
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        assertElements(info);
        /**/
    }


    @Test
    public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
        assertText("What We Do");
    }

    @Test
    public void buttonVisitSiteEnjoyHint(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.linkText("Visit EnjoyCSS site"));

    }

    @Test
    public void buttonViewDemo(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1728')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyCSS);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[@title='Visit site']"));
        goToURL(currentWindows, "http://enjoycss.com/");
    }
}
