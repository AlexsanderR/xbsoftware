package XBSoftware_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsTest extends XBSoftware_Basic.SpecMethodAndVariables{
	
	 //@Test linkProducts
	  	  public String textProductsPage = "Our products";

	 //@Test technologies                          
	  public By linkCSS3Inactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(1)");
	  public By linkCSS3WebixActive = By.cssSelector("body > section.section-products > div > div > div.col-sm-7.tech_post > div > a.active");
	  public By linkCSS3Active = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	
	  public By linkHTML5Inactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(2)");
	  public By lincHTML5StaffManagerActive = By.cssSelector("body > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a.active");
	  public By linkHTML5Active = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	  
	  public By linkJavaScriptInactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(3)");
	  public By linkJavaScriptBtrackActive = By.cssSelector("body > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a.active");
	  public By linkJavaScriptActive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	  
	  public By linkJqueryInactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(4)");
	  public By linkJqueryStaffManagerActive = By.cssSelector("body > section.section-products > div > div > div.col-sm-7.tech_post > div > a.active");
	  public By linkJqueryActive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	 
	  public By linkPHPInactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(5)");
	  public By linkPHPBtrackActive = By.cssSelector("body > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a.active");
	  public By linkPHPActive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	 
	  public By linkScrumInactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(6)");
	  public By linkScrumWebixActive = By.cssSelector("body > section.section-products > div > div:nth-child(5) > div.col-sm-7.tech_post > div > a.active"); 
	  public By linkScrumActive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");

      public By linkWebixInactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(7)");
	  public By linkWebixStaffManagerActive = By.cssSelector("body > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a.active");
	  public By linkWebixActive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	 
	  public By linkYiiFrameworkInactive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(8)");
	  public By linkYiiFrameworkStaffManagerActive = By.cssSelector("body > section.section-products > div > div > div.col-sm-7.tech_post > div > a.active");
	  public By linkYiiFrameworkActive = By.cssSelector("body > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active");
	  
	
	  
	  
	@Test 
	public void linkProducts(){
		findElementClick(linkProducts);
		assertText(textProductsPage);
	}
	
	@Test
	public void productsLinkXBSoftware(){
		findElementClick(linkProducts);
		findElementClick(By.linkText("XB Software"));
		assertText(whatWeDo);
	}
	
	@Test
	public void linkGoHomePage(){
		findElementClick(linkProducts);
		findElementClick(goToMainPage);
		assertText(whatWeDo);
	}
	
	@Test
	public void arrowUp(){
		findElementClick(linkProducts);
		findElementClick(buttonUp);
	}
	
	@Test
	public void emailAddress(){
	findElementClick(linkProducts);
	assertElements(info);
	}
	
	@Test
	public void technologies(){
	findElementClick(linkProducts);
	findElementClick(linkCSS3Inactive);
	assertElements(linkCSS3WebixActive);
	findElementClick(linkCSS3Active);
	assertElements(linkCSS3Inactive);
	
	findElementClick(linkHTML5Inactive);
	assertElements(lincHTML5StaffManagerActive);
	findElementClick(linkHTML5Active);
	assertElements(linkHTML5Inactive);
	
	findElementClick(linkJavaScriptInactive);
	assertElements(linkJavaScriptBtrackActive);
	findElementClick(linkJavaScriptActive);
	assertElements(linkJavaScriptInactive);
	
	findElementClick(linkJqueryInactive);
	assertElements(linkJqueryStaffManagerActive);
	findElementClick(linkJqueryActive);
	assertElements(linkJqueryInactive);
	
	findElementClick(linkPHPInactive);
	assertElements(linkPHPBtrackActive);
	findElementClick(linkPHPActive);
	assertElements(linkPHPInactive);
	
	findElementClick(linkScrumInactive);
	assertElements(linkScrumWebixActive);
	findElementClick(linkScrumActive);
	assertElements(linkScrumInactive);
	
	findElementClick(linkWebixInactive);
	assertElements(linkWebixStaffManagerActive);
	findElementClick(linkWebixActive);
	assertElements(linkWebixInactive);
	
	findElementClick(linkYiiFrameworkInactive);
	assertElements(linkYiiFrameworkStaffManagerActive);
	findElementClick(linkYiiFrameworkActive);
	assertElements(linkYiiFrameworkInactive);
	
	}
    @Test
    public void buttonViewDemoWebix(){
        findElementClick(linkProducts);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[@href='http://webix.com/demos/']"));
        goToURL(currentWindows, "http://webix.com/demos/");
    }
	
	@Test 
	public void buttonViewDemoStaffManager(){
	findElementClick(linkProducts);
	String currentWindows = driver.getWindowHandle();
	findElementClick(By.xpath("//a[@href='/demos/staff-manager/']"));
	goToURL(currentWindows, "/demos/staff-manager/");
	}
	
	@Test 
	public void buttonViewDemoBtrack(){
	findElementClick(linkProducts);
	String currentWindows = driver.getWindowHandle();
	findElementClick(By.xpath("//a[@href='/demos/btrack']"));
	goToURL(currentWindows, "http://xbsoftware.com/demos/btrack/login.php");
	}

    @Test
    public void buttonViewDemoEnjoyHint(){
        findElementClick(linkProducts);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/demos/enjoyhint/']"));
        goToURL(currentWindows, "http://xbsoftware.com/demos/enjoyhint/");
    }
	

	
	@Test
	public void requestAQuote(){
		findElementClick(linkProducts);
		findElementClick(By.xpath("/html/body/section[3]/div/div[1]/div[2]/a[1]"));
		assertText("Request a quote");
		findElementClick(linkProducts);
		waitForElement(linkProducts);
		findElementClick(By.xpath("/html/body/section[3]/div/div[2]/div[2]/a[1]"));
		assertText("Request a quote");
		findElementClick(linkProducts);
		waitForElement(linkProducts);
		findElementClick(By.xpath("/html/body/section[3]/div/div[3]/div[2]/a[1]"));
		assertText("Request a quote");
				
	}
	
}
