package tools;

import net.lightbody.bmp.proxy.ProxyServer;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.UnknownHostException;

public class WebProxyTools {

    private static ProxyServer server;

    public static void startProxy() {
        server = new ProxyServer(4444);
        try {
            server.start();
            server.setCaptureHeaders(true);
            server.setCaptureContent(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DesiredCapabilities getCapabilities() {
        DesiredCapabilities capabilities = null;
        Proxy proxy;
        try {
            proxy = server.seleniumProxy();
            capabilities = new DesiredCapabilities();
            capabilities.setCapability(CapabilityType.PROXY, proxy);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return capabilities;
    }

    public static ProxyServer getServer() {
        return server;
    }

    public static void stopProxy() {
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
