package tools;

import java.util.ArrayList;
import java.util.List;

public class ListUrl {

    private static ListUrl listUrl;

    private static List<DescriptionUrl> descriptionUrlList;

    public static List<DescriptionUrl> getDescriptionUrlList() {
        return descriptionUrlList;
    }

    private ListUrl() {
        descriptionUrlList = new ArrayList<DescriptionUrl>();
    }

    public static ListUrl createList(){
        if(listUrl == null) {
            listUrl = new ListUrl();
        }
        return listUrl;
    }

    public static boolean addUrl(String url, String browser) {
        DescriptionUrl descriptionUrl = new DescriptionUrl(url, browser);
        if(descriptionUrlList.isEmpty()){
            descriptionUrlList.add(descriptionUrl);
            return true;
        }
        if(!descriptionUrlList.contains(descriptionUrl)) {
            descriptionUrlList.add(descriptionUrl);
            return true;
        }
        return false;
    }

    public static class DescriptionUrl {

        String url;
        String browser;

        public String getUrl() {
            return url;
        }

        public String getBrowser() {
            return browser;
        }

        DescriptionUrl(String url, String browser) {
            this.url = url;
            this.browser = browser;
        }
    }
}
