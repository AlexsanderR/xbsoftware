package tools;

import GlobalMethod.GlobalMethod;
import net.lightbody.bmp.core.har.Har;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;

public class HARTools {

    private static final String HARSTORAGE_URL = "http://192.168.3.244:5000/results/upload";

    public static void sendHAR(String path){
        HttpClient httpclient = new DefaultHttpClient();
        httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

        HttpPost httppost = new HttpPost(HARSTORAGE_URL);
        MultipartEntity mpEntity = new MultipartEntity();
        ContentBody cbFile = new FileBody(new File("C:\\Harfiles\\" + path));

        mpEntity.addPart("file", cbFile);
        httppost.setEntity(mpEntity);
        HttpResponse response;
        try {
            response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {
                System.out.println(EntityUtils.toString(resEntity));
            }
            if (resEntity != null) {
                resEntity.consumeContent();
            }

            httpclient.getConnectionManager().shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String saveHAR(Har har) {
        String fileName = GlobalMethod.randomText(5) + ".har";
        File file = new File("C:\\Harfiles\\" + fileName);
        try {
            har.writeTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;

    }
}
