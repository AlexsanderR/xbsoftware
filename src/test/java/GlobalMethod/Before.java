package GlobalMethod;

import XBSoftware_Tests.MainPageTest;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.testng.annotations.BeforeSuite;
import tools.ListUrl;

import java.io.File;
import java.io.IOException;

public class Before {

    private static Logger LOG = Logger.getLogger(Before.class);

    @BeforeSuite
    public void beforeSuite() {
        ListUrl.createList();
        try {
            FileUtils.cleanDirectory(new File("./resources/screenshots/"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
