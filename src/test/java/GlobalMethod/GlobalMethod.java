package GlobalMethod;

import XBSoftware_Tests.MainPageTest;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;

//import com.thoughtworks.selenium.Wait;

public class GlobalMethod extends Before{

    public WebDriver driver;
    static Logger LOG = Logger.getLogger(MainPageTest.class);
   /* private static String BrowserType = System.getProperty("");*/

    public enum Browser {
        Firefox,
        IE,
        Chrome,
        Safari
    }

    //move curcor on element
    public void moveCursorToElement(By element) {
        //driver.findElement(By.xpath("element1")).click();
        new Actions(driver).moveToElement(driver.findElement(element)).perform();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //new Actions(driver).moveToElement(driver.findElement(By.xpath("//*[@id='menu-item-450']/a"))).click().perform();
    }

    //method for select browser in console: mvn test -Dbrowser=chrome
    /*public WebDriver createWebDriver() {
        if (driver != null) return driver;
        else {
            if (BrowserType.equals("firefox"))
                return new FirefoxDriver();

		            if (BrowserType.equals("opera")) {

		            	 driver = new OperaDriver();
		                 return new OperaDriver();

		            }

            if (BrowserType.equals("ie")) {
                System.setProperty("webdriver.ie.driver", "c://iedriver//IEDriverServer.exe");
                return new InternetExplorerDriver();
            }

            if (BrowserType.equals("chrome")) {

                System.setProperty("webdriver.chrome.driver", "c://chromedriver//chromedriver.exe");
                driver = new ChromeDriver();
                return driver;

            }

            if (BrowserType.equals("safari"))
                return new SafariDriver();

            else
                return new FirefoxDriver();

        }
    }*/

    public WebDriver getWebDriver (String browser){
        switch (Browser.valueOf(browser)){
            case Firefox:
                return new FirefoxDriver();
            case IE:
                return new InternetExplorerDriver();
            case Chrome:
                return new ChromeDriver();
            case Safari:;
                return new SafariDriver();
            default:
                return new FirefoxDriver();
        }
    }
    public WebDriver getWebDriver (String browser, DesiredCapabilities capabilities){
        switch (Browser.valueOf(browser)){
            case Firefox:
                return new FirefoxDriver(capabilities);
            case IE:
                return new InternetExplorerDriver(capabilities);
            case Chrome:
                return new ChromeDriver(capabilities);
            case Safari:
                return new SafariDriver(capabilities);
            default:
                return new FirefoxDriver(capabilities);
        }
    }


    //Method random of numbers
    public static String ramdomNumber() {
        Integer max = 23000;
        Integer number1 = (int) (Math.random() * (max - 1) + 1);
        String number = number1.toString();


        return number;
    }

    //Method random text
    public static String randomText(int lengthOfText) {
        String string = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        String[] abc = string.split(",");
        Integer max = abc.length;
        String text = "";
        for (int i = 0; i < lengthOfText; i++) {
            Integer number = (int) (Math.random() * (max - 1) + 1);
            text += abc[number];
        }
        return text;


    }

    // Method for test if checkbox is checked
    public void checkedElements(By findElement) {
        WebElement element = driver.findElement(findElement);
        if (element.getAttribute("checked") != null) {
            String str = "ok";
        } else {
            String className = getClass().toString();
            LOG.error("Element '" + findElement + "' unchecked in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
        }
    }

    // Method for test if checkbox is unchecked
    public void uncheckedElements(By findElement) {
        WebElement element = driver.findElement(findElement);
        if (element.getAttribute("checked") == null) {
            String str = "ok";
        } else {
            String className = getClass().toString();
            LOG.error("Element '" + findElement + "' checked in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
        }
    }

    // Method to find elements
    public void assertElements(By findElement) {
        try {
            WebElement element = driver.findElement(findElement);


        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();

        }
    }

    // Method to find elements and click
    public void findElementClick(By findElement)  {
        try {
            WebElement element = driver.findElement(findElement);
            element.click();

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'" );
            errorScreenshot();

        }
    }

    //Method for testing asserts
    public void assertElement(String msg, String element) {
        try {
            Assert.assertTrue(msg, driver.getPageSource().contains(element));
        } catch (AssertionError e) {
            String className = getClass().toString();
            LOG.error(msg + "'" + element + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            throw e;
        }
    }

    //Method for assert text
    public void assertText(String element) {
        assertElement("Assert Text error: ", element);
    }

    //Method for get current title of method
    public static String getMethodName(String className) {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

        for (int i = 0; i < ste.length; i++) {
            System.out.println(ste[i].getClassName() + ", " + className + ", " + ste[i].toString());
            if (className.contains(ste[i].getClassName())) {
                return ste[i].getMethodName();
            }
        }
        return "Unknown";
    }


    //Method for going to url in new tabs
    public void goToURL(String currentWindows, String url) {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        assertURL(url);
        driver.close();
        driver.switchTo().window(currentWindows);

    }

    //Method for assert url
    public void assertURL(String url) {
        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(url));
        } catch (AssertionError e) {
            String className = getClass().toString();
            LOG.error("Not opened url: '" + url + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            throw e;
        }
    }


    //Method for clear and fill  input fields
    public void inputField(By element, String msg) {
        try {
            WebElement input = driver.findElement(element);
            input.clear();
            input.sendKeys(msg);
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + element + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();

        }
    }

    // Timeouts for 5 seconds
    public void timeout() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Methods for select elements in drop down menu
    public void selectOption(String value) {
        try {
            List<WebElement> allSelects = driver.findElements(By.tagName("select"));
            for (WebElement select : allSelects) {
                List<WebElement> allOptions = select.findElements(By.tagName("option"));
                for (WebElement option : allOptions) {
                    if (option.getAttribute("value").contains(value)) {
                        option.click();
                        return;
                    }
                }
            }
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found select option '" + value + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
        }
    }

    //For method waitForElement
    public ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement toReturn = driver.findElement(locator);
                if (toReturn.isDisplayed()) {
                    return toReturn;
                }
                return null;
            }
        };
    }

    //Method waits for download elements
    public void waitForElement(By element) {
        Wait<WebDriver> wait = new WebDriverWait(driver, 30);
        WebElement elementWait = wait.until(visibilityOfElementLocated(element));
    }


    // method for upload file
    public void uploadedFile(By findElement, String filePath) {
        try {
            WebElement element = driver.findElement(findElement);
            File file = new File(filePath);
            String fileName = file.getAbsolutePath();
            element.sendKeys(fileName);

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("File '" + filePath + "' did not upload.... in  '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
        }
    }

    public void assertTextOnPage(By findElement, String text) {
        WebElement element = driver.findElement(findElement);
        waitForElement(findElement);
        String elem = element.getText();
        if (!elem.equals(text)) {
            LOG.error("Error");
        }

    }

    // for check text by errors
    public void checkToError(String error, String textError) {
        if (!error.equals(textError)) {
            String className = getClass().toString();
            LOG.error("Error not correct...  in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
        }
    }

    // for DND elements
    public void dndElements(By dragElement, By targetElement){
        try {
            WebElement draggable = driver.findElement(dragElement);
            String drag = draggable.toString();
            WebElement target = driver.findElement(targetElement);
            new Actions(driver).clickAndHold(draggable).moveToElement(target).release().perform();
        }
        catch(Exception e){
            String className = getClass().toString();
            LOG.error("Can not dnd elements in class "   + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
        }
    }

    // for makes screenshots
    public void errorScreenshot( ){
        try {
            String className = getClass().toString();
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String path = "./resources/screenshots/" + className + "_" + getMethodName(className) + ".png"; // screenshot.getName(); -> screenshot6132534184894364942.png
            FileUtils.copyFile(screenshot, new File(path));
        }
        catch(Exception e)
        {}


    }
}

