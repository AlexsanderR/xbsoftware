package XBSoftware_Basic;

//package XBSoftware_Basic;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import tools.ListUrl;


public class SpecMethodAndVariablesRU extends GlobalMethod.GlobalMethod {

    public String globalUR_RU = "http://xbsoftware.ru/";
    public static final Logger LOG = Logger.getLogger(SpecMethodAndVariablesRU.class);
    private String browser;

  /* @Parameters("browser")*/
    @BeforeMethod
    public void beforeTest(/*String browser*/) {
        /*this.browser = browser;
        LOG.info(browser);*/
        driver = getWebDriver("Firefox");
        driver.manage().window().maximize();
        driver.get(globalUR_RU);
    }


    @AfterMethod
    public void afterTest() {
        ListUrl.addUrl(driver.getCurrentUrl(), browser);
        driver.close();
    }


}
