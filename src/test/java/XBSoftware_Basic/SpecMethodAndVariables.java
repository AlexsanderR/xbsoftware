package XBSoftware_Basic;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;


public class SpecMethodAndVariables extends GlobalMethod.GlobalMethod {



    public static void deleteAllFiles() throws IOException {
        FileUtils.cleanDirectory(new File("./resources/screenshots/"));
    }

    @Parameters("browser")
    @BeforeMethod
    public void beforeTest() {

        //if start tests in console: mvn test -Dbrowser=firefox
        //driver = createWebDriver();
        driver = getWebDriver("Firefox");
      //  System.setProperty("webdriver.chrome.driver", "c://chromedriver//chromedriver.exe");
      //  driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(globalURL);
    }

	 /* Main page */

    // @Test whatWeDo, readNore

    public String globalURL = "http://xbsoftware.com/";
   // public String globalURL = "http://webix.com/ru/demo/datatable/dnd-for-rows/";

    public By ReadMore = By.xpath("/html/body/section[3]/div/div/div[2]/a/div[3]");
    public String readMoreAssert = "The IT Services You Need";
    public By imageWhatWeDo = By.xpath("/html/body/section[3]/div/div/div[2]/a");

    //@Test carousel
    public By carouselRight = By.xpath("//*[@id=\"myCarousel\"]/a[2]/span");
    public String carouselLastSlideAssert = "Handle Huge Data Amounts";
    public By carouselLeft = By.xpath("//*[@id=\"myCarousel\"]/a[1]/span");
    public By carouselSlide = By.className("border-radius-10");
    public String carouselFirstSlideAssert = "We have proven expertise";

    //@Test linGokWebix
    public By linkGoToWebix = By.linkText("Webix");
    public String linkWebix = "http://webix.com/";

    // @Test up
    public By buttonUp = By.id("arrow-up");

    // @Test testimonials
    public By testimonialsLink = By.linkText("More Testimonials");
    public String testimonialsAssert = "Testimonials";

    //@Test information
    public By info = By.linkText("info@xbsoftware.com");
	  

	  /* AboutUs_Testimonials */

    //@Test linkTestimonials
    public By linkAboutUs = By.xpath("//*[@id='menu-item-21']/a");
    public By linkTestimonials = By.xpath("//*[@id='menu-item-638']/a");
    public String testimonials = "Testimonials";

    //@Test softwareDevelopmentCompany
    public By softwareDevelopmentCompany = By.xpath("/html/body/div[4]/div[2]/div/div[2]/p/a");
    public String whatWeDo = "What We Do";
	  
	  /*AboutUs_TechnologyExpertise*/
      public By linkTechnologyExpertise = By.xpath("//*[@id='menu-item-639']/a");
    public String technologyExpertise = "Technology Expertise";

    //@Test linkTechnologyExpertise


    //@Test linkXBSoftware
    public By linkXBSoftware = By.xpath("/html/body/div[4]/div[2]/div/div[2]/p/a");

    //@Test requestAQuote
    public By requestAQuote = By.className("to-request-btn");
    public String requestaQuote = "Request a quote";
	  
	  
	  /*Services*/

    //@Test linkServices
    public By linkServices = By.xpath("//*[@id=\"menu-item-24\"]/a");
    public String textServicePage = "The IT Services You Need";

    //@Test linkXBSoftware
    public By linkXB_Software = By.xpath("//a[@title='software development company']");

    //@Test linkReadMore
    public By linkReadMore = By.linkText("Read more");

    //@Test servicesWebDevelopment
    public By servicesWebDevelopment = By.xpath("//*[@id=\"menu-item-640\"]/a");
    public String textServicesWebDevelopment = "Web Development";

    //@Test servicesBusinessAnalysis
    public By servicesBusinessAnalysis = By.xpath("//*[@id=\"menu-item-641\"]/a");
    public String textServicesBusinessAnalysis = "Business Analysis";

    //@Test servicesQualityAssurance
    public By servicesQualityAssurance = By.xpath("//*[@id=\"menu-item-642\"]/a");
    public String textServicesQualityAssurance = "Quality Assurance";
	  
	  
	
	  
	 /*Portfolio*/

    public By linkPortfolio = By.xpath("//*[@id=\"menu-item-644\"]/a");
	  
	  
	  /*Products*/

    public By linkProducts = By.xpath("//*[@id=\"menu-item-505\"]/a");
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  /* AboutUs */

    //Test linkSoftwareDevelopmentCompany
    public By linkSoftwareDevelopmentCompany = By.linkText("software development company");
    public String assertSoftwareDevelopmentCompany = "We Use";

    //@Test linkBusinesAnalysis
    public By linkBusinesAnalysis = By.linkText("business analysis");
    public String assertBusinesAnalysis = "Business Analysis";

    //@Test linkWebDevelopment
    public By linkWebDevelopment = By.linkText("web development");
    public String asserWebDevelopment = "Web Development";

    //@Test linkQualityAssurance
    public By linkQualityAssurance = By.linkText("quality assurance");
    public String assertQualityAssurance = "Quality Assurance";

    //@Test goToMainPage
    public By goToMainPage = By.xpath("/html/body/header/div[2]/div[2]/div/a/span");


    //@Test sendMessage
    public By inputName = By.id("inputName");
    public String inputNameText = "Test";
    public By inputEmail = By.id("inputEmail");
    public String inputEmailText = "test@test.test";
    public By inputPhone = By.id("inputPhone");
    public String inputPhoneNumber = "111111111";
    public By inputMessage = By.id("inputMessage");
    public String inputMessageText = "b9pv1j2yzm1jsqritazk";
    public By send = By.xpath("//*[@id=\"wpcf7-f90-p4-o1\"]/form/div[3]/div[2]/div/input");
    public String sendText = "Your message was sent successfully. Thanks";
    public String assertWithoutFill = "This field is required.";
    public String sendIncorrectEmail = "test";
    public String sendIncorrectPhone = "test";
    public String assertIncorrectEmail = "Please enter a valid email address.";
    public String assertIncorrectPhone = "Please enter a valid phone number.";


    @AfterMethod
    public void afterTest() {
        driver.close();
    }
}