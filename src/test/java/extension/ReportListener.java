package extension;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;
import tools.HARTools;
import tools.ListUrl;
import tools.WebProxyTools;

import java.util.ArrayList;
import java.util.List;

public class ReportListener implements IReporter {

    private static final Boolean report = false;

    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> iSuites, String s) {
        if (report) {
            WebProxyTools.startProxy();
            List<String> strings = containsUrl(ListUrl.getDescriptionUrlList());
            for (String url : strings) {
                WebDriver webDriver = new FirefoxDriver(WebProxyTools.getCapabilities());
                WebProxyTools.getServer().newHar("xbsoftware.com");
                webDriver.get(url);
                String name = HARTools.saveHAR(WebProxyTools.getServer().getHar());
                HARTools.sendHAR(name);
                webDriver.close();
            }
            WebProxyTools.stopProxy();
        }
    }

    private List<String> containsUrl(List<ListUrl.DescriptionUrl> descriptionUrls) {
        List<String> strings = new ArrayList<String>();
        for (ListUrl.DescriptionUrl descriptionUrl : descriptionUrls) {
            if (strings.isEmpty()) {
                strings.add(descriptionUrl.getUrl());
            } else if (!strings.contains(descriptionUrl.getUrl())) {
                strings.add(descriptionUrl.getUrl());
            }
        }
        return strings;
    }
}
