package XBSoftware_RU_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class AboutUs_TestimonialsRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	 By linkAboutUs = By.xpath("//*[@id='menu-item-232']/a");
	 By linkTestimonials = By.xpath("//*[@id='menu-item-627']/a");
	 By buttonUp = By.id("arrow-up");
	 By info = By.linkText("info@xbsoftware.com");
	 By goToMainPage = By.xpath("//a[@href='http://xbsoftware.ru ']");
	

	@Test
	public void linkTestimonials(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-627')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		assertText("Отзывы");
	}

	@Test
	public void softwareDevelopmentCompany(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-627')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		findElementClick(By.xpath("//a[contains(text(),'компания-разработчик программного обеспечения')]"));
		assertText("Наши услуги");
	}
	
	@Test 
	public void requestAQuote(){((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-627')[0].parentNode.style.display = 'block'");
        findElementClick(linkTestimonials);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@href='/obratnaya-svyaz/']"));
		goToURL(currentWindows, "http://xbsoftware.ru/obratnaya-svyaz/");
		assertText("Свяжитесь с нами");
	}
	
	@Test 
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-627')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		findElementClick(buttonUp);
	}
	
	@Test
	public void goHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-627')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		findElementClick(goToMainPage);
		assertText("Наши услуги");
	}
		
	@Test
	public void information(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-627')[0].parentNode.style.display = 'block'");
		findElementClick(linkTestimonials);
		assertElements(info);		
	}
		
}
