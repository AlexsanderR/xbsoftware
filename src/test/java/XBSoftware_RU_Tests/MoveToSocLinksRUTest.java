package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class MoveToSocLinksRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	
	
	@Test
	public void LinkToGoogle(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='icon32 google_plus']"));
		goToURL(currentWindows, "https://plus.google.com/+Xbsoftware/posts");
		
	}
	
	@Test
	public void LinkToFacebook(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='icon32 facebook']"));
		goToURL(currentWindows, "https://www.facebook.com/xbsoftware");
	}
	
	@Test
	public void LinkToTwitter(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='icon32 twitter']"));
		goToURL(currentWindows, "https://twitter.com/xbsoftware");
	}
	
	@Test
	public void LinkToLinkedin(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='icon32 linkedin']"));
		goToURL(currentWindows, "http://www.linkedin.com/company/xb-software-ltd-?trk=top_nav_home");
	}
	
	@Test
	public void LinkToRss(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='icon32 rss ']"));
		goToURL(currentWindows, "http://xbsoftware.ru/blog/feed/");
	}

}
