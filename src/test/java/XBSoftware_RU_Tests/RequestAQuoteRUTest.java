package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class RequestAQuoteRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	By linkRequestAQuote = By.xpath("//a[@href='http://xbsoftware.ru/obratnaya-svyaz/']");
	By attachFile = By.name("userfile_1");
	By buttonSend = By.xpath("//input[@value='Отправить']");
	
	
	@Test
	public void openRequestAQuote(){
		findElementClick(linkRequestAQuote);
		assertText("Свяжитесь с нами");
	}
	
	@Test
	public void attachFile(){
		findElementClick(linkRequestAQuote);
		uploadedFile(attachFile, "src/test/filesForUplod/412.png");
		timeout();
		assertText("412.png");
		timeout();
		findElementClick(By.xpath("//span[@class='icon icon-delete']"));		
	}//
	
	@Test
	public void sendMessage(){
		findElementClick(linkRequestAQuote);
		findElementClick(buttonSend);
		assertText("Поле, обязательное для заполнения");
		
		inputField(By.id("inputName"), "Test");
		assertText("Поле, обязательное для заполнения");
		findElementClick(buttonSend);
		
		inputField(By.id("inputEmail"), "test");
		assertText("Пожалуйста, введите правильный email");
		
		inputField(By.id("inputEmail"), "test@test.test");
		assertText("Поле, обязательное для заполнения");
		findElementClick(buttonSend);
		
		inputField(By.id("inputPhone"), "test");
		assertText("Пожалуйста, введите правильный номер телефона");
		
		inputField(By.id("inputPhone"), "1111111");
		assertText("Поле, обязательное для заполнения");
		findElementClick(buttonSend);
		
		inputField(By.id("inputCompany"), "company");
		assertText("Поле, обязательное для заполнения");
		findElementClick(buttonSend);
		
		inputField(By.id("inputMessage"), "b9pv1j2yzm1jsqritazk");
        uncheckedElements(By.xpath("//input[@type='checkbox'][@value='Пришлите мне соглашение о неразглашении информации']"));
        findElementClick(By.xpath("//input[@type='checkbox'][@value='Пришлите мне соглашение о неразглашении информации']"));
		uploadedFile(attachFile, "src/test/filesForUplod/412.png");
		timeout();
		findElementClick(buttonSend);
		timeout();
		assertText("Ошибка валидации. Пожалуйста, проверьте правильность заполнения полей формы и отправьте еще раз.");
	}

    @Test
    public void up(){
        findElementClick(linkRequestAQuote);
        findElementClick(By.id("arrow-up"));

    }
}
