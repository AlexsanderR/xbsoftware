package XBSoftware_RU_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsXBStaffManagerRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	By linkProducts = By.xpath("//*[@id='menu-item-516']/a");
	By linkXBStaffManager = By.xpath("//*[@id='menu-item-750']/a");
	
	
	@Test 
	public void linkProductsXBStaffManager(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		assertText("Кастомизируемая система управления персоналом");
	}

    @Test
    public void linkGoHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        assertElements(By.linkText("info@xbsoftware.com"));
    }

    @Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
		assertText("Наши услуги");
	}
	
	@Test
	public void buttonGetYourFreeQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBStaffManager);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='btn btn-send']"));
		goToURL(currentWindows, "http://xbsoftware.ru/obratnaya-svyaz/");
		assertText("Свяжитесь с нами");
	}
	

    @Test
    public void carusel(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        findElementClick(By.cssSelector(".roundabout-holder a"));
        //timeout();
        findElementClick(By.cssSelector(".mfp-arrow-right"));
        assertElements(By.cssSelector(".mfp-counter:contains('2 of 3')"));
        findElementClick(By.cssSelector(".mfp-arrow-left"));
        assertElements(By.cssSelector(".mfp-counter:contains('1 of 3')"));
        findElementClick(By.cssSelector(".mfp-close"));
        assertText("Кастомизируемая система управления персоналом");
    }

    @Test
    public void findOutMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-750')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBStaffManager);
        findElementClick(By.linkText("Подробнее"));
        assertText("Эффективная система управления персоналом и ресурсами");
    }
}
