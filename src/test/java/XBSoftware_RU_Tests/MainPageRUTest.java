package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class MainPageRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	 //@Test carousel
	  public By carouselRight = By.xpath("//*[@id=\"myCarousel\"]/a[2]/span");
	  public By carouselLeft = By.xpath("//*[@id=\"myCarousel\"]/a[1]/span");
	  public By carouselSlide = By.className("border-radius-10");
	 
	  // @Test up
	  public By buttonUp = By.id("arrow-up");
	  
	  //@Test information
	  public By info = By.linkText("info@xbsoftware.com");
	
	  
	  
		
	@Test
	public void whatWeDo()  {
		findElementClick (By.xpath("//div[@class='center-part centered']"));
		assertText("ИТ-услуги, которые Вам нужны");
	}
	
	@Test
	public void readMore() {
		findElementClick(By.xpath("//div[@class='bottom-part btn-cream']"));   //div[contains(text(), 'Подробнее')]
		assertText("ИТ-услуги, которые Вам нужны");
	}
	
	
	@Test
	public void carousel(){
		findElementClick(carouselLeft);
		assertText("Визуализация большого количества данных");
		timeout();
		for (int i=0; i<3; i++){
		findElementClick(carouselRight);
		timeout();}
		findElementClick(carouselSlide);
		assertText("Мы умеем работать с пространственными географическими данными");		
	}
	
	@Test 
	public void linkGoWebix(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@href='http://webix.com/ru/']"));
		goToURL(currentWindows, "http://webix.com/ru/");
	} 
	
	@Test
	public void up(){
		findElementClick(buttonUp);
				
	}
	
	@Test
	public void testimonials(){
		findElementClick(By.xpath("//a[@class='pull-right']"));
		assertText("Отзывы");
	}
	
	@Test
	public void information(){
		assertElements(info);
		
	}
	
	@Test
	public void goToEN(){
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@href='http://xbsoftware.com/']"));
		goToURL(currentWindows, "http://xbsoftware.com/");
	}

    @Test
    public void text(){
        assertElements(By.xpath("//*[.='Наши новости']"));
        assertElements(By.xpath("//*[.='IT мероприятия']"));
        assertElements(By.xpath("//*[.='Сферы деятельности']"));
        assertElements(By.xpath("//*[.='Twitter']"));
    }

    @Test
    public void more(){
        findElementClick(By.linkText("Подробнее"));
        assertText("Теги");
    }

    @Test
    public void privacyPolicy(){
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.linkText("Политика конфиденциальности"));
        assertElements(By.cssSelector("span:contains('Политика Конфиденциальности')"));
    }
	
	
}
