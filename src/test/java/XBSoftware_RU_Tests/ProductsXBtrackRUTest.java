package XBSoftware_RU_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsXBtrackRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	
	By linkProducts = By.xpath("//*[@id='menu-item-516']/a");
	By linkXBtrack = By.xpath("//*[@id='menu-item-751']/a");
	
	@Test 
	public void linkProductsXBtrack(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		assertText("Гибкая система управления задачами и багтрэкер");
	}//

    @Test
    public void linkGoHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBtrack);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBtrack);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
        findElementClick(linkXBtrack);
        assertElements(By.linkText("info@xbsoftware.com"));
    }
	
	@Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
		assertText("Наши услуги");
	}
	
	@Test
	public void buttonGetYourFreeQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='btn btn-send']"));
		goToURL(currentWindows, "http://xbsoftware.ru/obratnaya-svyaz/");
		assertText("Свяжитесь с нами");
	}
	
	@Test
	public void buttonViewDemo(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
		findElementClick(linkXBtrack);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@title='Демо']"));
		goToURL(currentWindows, "http://xbsoftware.ru/demos/btrack/login.php");
		//assertText("XBtrack - Task and Bug Tracking System");
	}	

}
