package XBSoftware_RU_Tests;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class AboutUs_JobsRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU {
 
	By linkAboutUs = By.xpath("//li[@id='menu-item-232']");
	By linkJobs = By.xpath("//*[@id='menu-item-812']/a");
    By link = By.cssSelector(". dropdown-menu[style*=\"display: block\"] li[id='menu-item-812']");
    //By.cssSelector(".webix_view.webix_window[style*=\"display: block\"] .webix_win_body .webix_cal_row div:nth-child(6)")
	
	
	@Test
	public void linkJobs (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
		findElementClick(linkJobs);
		assertText("О нас");
	}
	
	@Test
	public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
		findElementClick(linkJobs);
		findElementClick(By.xpath("//a[@href='http://xbsoftware.ru ']"));
		assertText("Наши услуги");
	}//
	
	@Test
	public void buttonRespond (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
 		findElementClick(linkJobs);
		findElementClick(By.xpath("//span[@data-caption='Отклик на вакансию PHP-программист']"));
		assertText("Откликнуться на вакансию");
		findElementClick(By.xpath("//input[@value='Отправить резюме']"));
		assertElements(By.xpath("//label[@for='inputName'][contains(text(), 'Поле, обязательное для заполнения')]"));
		inputField(By.id("inputName"), "test");
	    inputField(By.id("inputEmail"), "test");//sendIncorrectEmail
	    assertElements(By.xpath("//label[@for='inputEmail'][contains(text(), 'Пожалуйста, введите правильный email')]"));//assertIncorrectEmail
	    inputField(By.id("inputEmail"), "test@test.test");//inputEmailText
	    inputField(By.id("inputPhone"), "dsdf");//sendIncorrectPhone
	    assertElements(By.xpath("//label[@for='inputPhone'][contains(text(), 'Пожалуйста, введите правильный номер телефона')]"));//assertIncorrectPhone
	    inputField(By.id("inputPhone"), "8136661313");//inputPhoneNumber
	    assertElements(By.xpath("//label[@for='inputMessage'][contains(text(), 'Поле, обязательное для заполнения')]"));
	    inputField(By.id("inputMessage"), "b9pv1j2yzm1jsqritazk");//inputMessage
	    uploadedFile(By.name("userfile_1"), "src/test/filesForUplod/412.png");
	    findElementClick(By.xpath("//input[@value='Отправить резюме']"));
	}

    @Test
    public void whatTheHack(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
        findElementClick(linkJobs);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[contains(text(),'What The Hack')]"));
        goToURL(currentWindows, "http://wth.by/");
    }

    @Test
    public void Front(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
        findElementClick(linkJobs);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[contains(text(),'4Front митап')]"));
        goToURL(currentWindows, "http://xbsoftware.ru/tag/4front/");
    }

    @Test
    public void XBSoftwareTube(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
        findElementClick(linkJobs);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[contains(text(),'XBSoftwareTube')]"));
        goToURL(currentWindows, "https://www.youtube.com/user/XBSoftwareTube");
    }

    @Test
    public void weOnFacebook(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-812')[0].parentNode.style.display = 'block'");
        findElementClick(linkJobs);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[contains(text(),'Мы на Facebook')]"));
        goToURL(currentWindows, "https://www.facebook.com/xbsoftware/photos_stream");
    }

		 
	
}
