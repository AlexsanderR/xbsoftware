package XBSoftware_RU_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class AboutUs_TechnologyExpertiseRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	
	By linksAboutUs =By.xpath("//*[@id='menu-item-232']/a");
	By linkTechnologyExpertise  = By.xpath("//*[@id='menu-item-629']/a");
	By goToMainPage = By.xpath("//a[@href='http://xbsoftware.ru ']");
	By linkXBSoftware= By.xpath("//a[@href='http://xbsoftware.ru/']");
	By buttonUp = By.id("arrow-up");
	By requestAQuote = By.linkText("Свяжитесь с нами");
	String whatWeDo = "Наши услуги";
	String requestaQuote = "Свяжитесь с нами";
	
	@Test
	public void linkTechnologyExpertise(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-629')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		assertText("Технологии и опыт");
	}
	
	@Test
	public void goHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-629')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		findElementClick(goToMainPage);
		assertText(whatWeDo);
	}

	@Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-629')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		findElementClick(linkXBSoftware);
		assertText(whatWeDo);
		
	}
	
	@Test 
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-629')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		findElementClick(buttonUp);
	}
	
	@Test 
	public void requestAQuote(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-629')[0].parentNode.style.display = 'block'");
		findElementClick(linkTechnologyExpertise);
		String currentwindows = driver.getWindowHandle();
		findElementClick(requestAQuote);
		goToURL(currentwindows, "http://xbsoftware.ru/obratnaya-svyaz/");
		assertText(requestaQuote);
		
	}
}
