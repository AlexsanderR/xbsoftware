package XBSoftware_RU_Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;


public class PortfolioCaseStudiesRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	By linkPortfolio = By.xpath("//*[@id='menu-item-634']/a");
	By linkCaseStudies = By.xpath("//*[@id='menu-item-450']/a");
	By Arrows = By.id("arrow-up");                                       

	
	@Test
	public void linkCaseStudies(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		waitForElement(Arrows);
		assertText("Кейсы");
	}

	
	@Test 
	public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		waitForElement(Arrows);
		findElementClick(By.xpath("/html/body/div[1]/header/div[2]/div[2]/div/a/span"));
		assertText("Наши услуги");
	}

		
	@Test
	public void linkProjectsOverview (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(By.xpath("//a[@href='http://xbsoftware.ru/obzor-projektov/']"));
		assertText("Обзор проектов");
	}
	
	@Test
	public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(Arrows);
	}
	
	@Test
	public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		assertElements(By.linkText("info@xbsoftware.com"));
	}
	
	@Test
	public void buttonViewCaseWorkflowApplicationForBusinesses(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
        findElementClick(By.linkText("Приложение для управления рабочими процессами на предприятиях"));
        assertElements(By.xpath("//*[.='Приложение для управления рабочими процессами на предприятиях']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));
		
	}
	

	@Test
	public void buttonViewCaseOnlineShoppingService(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
        findElementClick(By.linkText("Онлайн-сервис для поиска и покупки товаров"));
        assertElements(By.xpath("//*[.='Онлайн-сервис для поиска и покупки товаров']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));
	}
	
	@Test
	public void buttonViewCaseWebBasedEducatorDevelopedSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
        findElementClick(By.linkText("Программное обеспечение для преподавателей"));
        assertElements(By.xpath("//*[.='Программное обеспечение для преподавателей']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));
	}

    @Test
    public void buttonViewCaseProjectManagementAndWorkflowCloudSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.linkText("Программное обеспечение для автоматизации бизнес-процессов"));
        assertElements(By.xpath("//*[.='Программное обеспечение для автоматизации бизнес-процессов']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));
    }

    @Test
    public void buttonViewCaseSocialPlatform (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
         findElementClick(By.linkText("Социальная платформа"));
        assertElements(By.xpath("//*[.='Социальная платформа']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));
    }

    @Test
    public void buttonViewCaseEmployeePlanningSystem(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.linkText("Система планирования работы сотрудников"));
        assertElements(By.xpath("//*[.='Система планирования работы сотрудников']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));

    }

    @Test
    public void buttonViewCaseProjectManagementToolGorCommunicationsPortal(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.linkText("Инструмент управления проектами для коммуникационного портала"));
        assertElements(By.xpath("//*[.='Инструмент управления проектами для коммуникационного портала']"));
        findElementClick(By.xpath("//*[.='Скачать кейс в PDF']"));


    }
	
	// test for Technologies.  Checked five elements, one for each project.
	
	@Test
	public void portfolioCaseStudiesTechnologiesClickActiveInactive (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
		findElementClick(linkCaseStudies);
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(5) > span")); 
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span")); 
		
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span")); 
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a:nth-child(2) > span")); 
		
		
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(21) > span"));  
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span")); 
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a:nth-child(4) > span"));
		
		
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(15) > span")); //SCRUM
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a.active > span"));
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div:nth-child(5) > div.col-sm-7.tech_post > div > a:nth-child(4) > span"));
		
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(1) > span"));//Amazon S3
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span"));
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div:nth-child(7) > div.col-sm-7.tech_post > div > a:nth-child(1) > span"));
		
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(14) > span"));//ruby on rails
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div.row.v-padding10 > div.col-sm-7.tech_post > div > a.active > span"));
		findElementClick(By.cssSelector("body > div.global > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));
		assertElements(By.cssSelector("body > div.global > section.section-products > div > div:nth-child(9) > div.col-sm-7.tech_post > div > a:nth-child(9) > span"));
		
		
	}

    @Test
    public void readMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-450')[0].parentNode.style.display = 'block'");
        findElementClick(linkCaseStudies);
        findElementClick(By.xpath("//a[@class='btn btn-send']"));
        findElementClick(By.xpath("//*[@class='text-link-to-pdf border-radius-10']"));

    }
}

