package XBSoftware_RU_Tests;

import org.apache.tools.ant.taskdefs.Java;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ServicesRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU {
	
	By linkYslugi = By.xpath("//*[@id='menu-item-233']/a");
	String whatWeDo = "Наши услуги";
	
	By Otpravitq = By.xpath("//input[@value='Отправить']");
	By inputEmail = By.xpath("//input[@name='user_email']");
	By inputPhone = By.xpath("//input[@name='user_phone']");
	
	
	@Test
	public void linkServices (){		
		findElementClick(linkYslugi);
		assertText("ИТ-услуги, которые Вам нужны");

	}

    @Test
    public void linkGoHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
        findElementClick(linkYslugi);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
        findElementClick(linkYslugi);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-751')[0].parentNode.style.display = 'block'");
        findElementClick(linkYslugi);;
        assertElements(By.linkText("info@xbsoftware.com"));
    }

	@Test
	public void linkReadMore(){
		findElementClick(linkYslugi);
		findElementClick(By.xpath("//a[@href='/tehnologii-i-opyt/']"));
		assertText("Технологии и опыт");
	}


	
	@Test
	public void servicesSendMessage(){
		findElementClick(linkYslugi);
		findElementClick(Otpravitq);
		assertText("Поле, обязательное для заполнения");
		inputField(By.xpath("//input[@name='user_name']"), "Test");
		inputField(inputEmail, "testtesttest");
		assertText("Пожалуйста, введите правильный email");
		inputField(inputEmail, "test@test.test");
		inputField(inputPhone, "sdfsdfsd");
		assertText("Пожалуйста, введите правильный номер телефона");
		inputField(inputPhone, "+375000000000");
		assertText("Поле, обязательное для заполнения");
		inputField(By.id("inputMessage"), "b9pv1j2yzm1jsqritazk");
		findElementClick(Otpravitq);
		timeout();
		assertText("Ошибка валидации. Пожалуйста, проверьте правильность заполнения полей формы и отправьте еще раз.");
	}
	
	@Test
	public void servicesWebDevelopment(){
		moveCursorToElement(linkYslugi);
		findElementClick(By.xpath("//*[@id='menu-item-630']/a")); //servicesWebDevelopment
		assertText("Веб-разработка");
	}
	
	@Test
	public void servicesBusinessAnalysis(){
		moveCursorToElement(linkYslugi);
		findElementClick(By.xpath("//*[@id='menu-item-631']/a")); //BusinessAnalysis
		assertText("Бизнес-Анализ");
	}
	
	@Test
	public void servicesQualityAssurance(){
		moveCursorToElement(linkYslugi);
		findElementClick(By.xpath("//*[@id='menu-item-632']/a")); //servicesQualityAssurance
		assertText("Контроль качества разработки");
	}

    @Test
    public void servicesInternetMarketing() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1479')[0].parentNode.style.display = 'block'");
        findElementClick(By.xpath("//*[@id='menu-item-1479']/a"));
        assertText("Интернет-маркетинг");
    }

    @Test
    public void servicesInternetMarketingSEO() {
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-233 ul')[0].style.display = 'block'");
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1479 ul')[0].style.display = \"block\";");
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"li[id='menu-item-1480'] a\").click()");
        assertText("Поисковая оптимизация (SEO)");

    }

    @Test
    public void servicesInternetMarketingSMM(){
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-233 ul')[0].style.display = 'block'");
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1479 ul')[0].style.display = \"block\";");
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"li[id='menu-item-1481'] a\").click()");
        assertText("Маркетинг в социальных сетях (SMM)");
    }

    @Test
    public void servicesInternetMarketingSMO(){
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-233 ul')[0].style.display = 'block'");
        ((JavascriptExecutor) driver).executeScript("jQuery('#menu-item-1479 ul')[0].style.display = \"block\";");
        ((JavascriptExecutor) driver).executeScript("document.querySelector(\"li[id='menu-item-1482'] a\").click()");
        assertText("Оптимизация сайта под социальные сети (SMO)");
    }

    @Test
    public void servicesReadMoreAudit(){
        findElementClick(linkYslugi);
        findElementClick(By.xpath("//a[@href='/uslugi/seo-audit-saita/']"));
        assertText("Полный SEO-аудит сайта");
    }

    @Test
    public void servicesReadMoreSEO(){
        findElementClick(linkYslugi);
        ((JavascriptExecutor)driver).executeScript("document.querySelector(\"a[href='/uslugi/poiskovaja-optimizatsija/']\").click()");
        assertText("Поисковая оптимизация (SEO)");
    }

    @Test
    public void servicesReadMoreCRO(){
        findElementClick(linkYslugi);
        findElementClick(By.xpath("//a[@href='/uslugi/optimizatsija-koeffitsienta-konversii/']"));
        assertText("Оптимизация коэффициента конверсии (CRO)");
    }

    @Test
    public void servicesReadMorePPC(){
        findElementClick(linkYslugi);
        ((JavascriptExecutor)driver).executeScript("document.querySelector(\"a[href='/uslugi/kontekstnaja-reklama-ppc/']\").click()");
        assertText("Контекстная реклама (PPC)");
    }

    @Test
    public void servicesReadMoreContentMarketing(){
        findElementClick(linkYslugi);
        findElementClick(By.xpath("//a[@href='/uslugi/kontent-marketing/']"));
        assertText("Контент-маркетинг");
    }

    @Test
    public void servicesReadMoreWebAnalystics(){
        findElementClick(linkYslugi);
        ((JavascriptExecutor)driver).executeScript("document.querySelector(\"a[href='/uslugi/web-analitika/']\").click()");
        assertText("Комплексная веб-аналитика");
    }

    By request = By.xpath("//input[@value='Заказать услугу по контент-маркетингу']");

    @Test
    public void requestWEbAnalistics(){
        findElementClick(linkYslugi);
        findElementClick(By.xpath("//a[@href='/uslugi/kontent-marketing/']"));
        assertText("Контент-маркетинг");
        waitForElement(request);
        findElementClick(request);
        assertText("Поле, обязательное для заполнения");
        inputField(By.id("inputName"), "test");
        assertText("Поле, обязательное для заполнения");

        inputField(By.id("inputEmail"), "test");
        assertText("Пожалуйста, введите правильный email");
        inputField(By.id("inputEmail"), "test@test.test");
        assertText("Поле, обязательное для заполнения");

        inputField(By.id("inputSite"), "test");
        assertText("Please enter a valid URL.");
        inputField(By.id("inputSite"), "http://xbsoftware.com/");
        assertText("Поле, обязательное для заполнения");

        inputField(By.id("inputMessage"), "test");
        findElementClick(request);
        timeout();
        assertText("Ошибка заполнения. Заполните все поля и отправьте снова.");
    }

}
