package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ContactUsRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	
		 By goToMainPage = By.xpath("//a[@href='http://xbsoftware.ru ']");
		 By ContactUs = By.xpath("//div/h2/span[contains(text(), 'Контакты')]");
		 By ZoomOut = By.xpath("//div[@title='Zoom out']");
		 By ZoomIn = By.xpath("//div[@title='Zoom in']");
		 By SendMessage = By.xpath("//input[@type='submit'][@value='Отправить']");
		 By contactUS = By.xpath("//*[@id='menu-item-234']/a");
		 
		 
		@Test 
		public void ContactUs(){
			findElementClick(contactUS);
			findElementClick (ContactUs);
			findElementClick (ContactUs);
			findElementClick(ZoomOut);
			findElementClick(ZoomOut);
			findElementClick(ZoomIn);
			findElementClick(ZoomIn);
		
	}
		@Test 
		public void ContactUsSendMessage(){
			findElementClick(contactUS);
			findElementClick(SendMessage);
			assertElements(By.xpath("//label[contains(text(),'Поле, обязательное для заполнения')]"));
			inputField(By.xpath("//input[@type='text'][@name='user_name']"), "test");
			inputField(By.xpath("//input[@type='email'][@name='user_email']"), "test");
			findElementClick(SendMessage);
			assertElements(By.xpath("//label[contains(text(),'Пожалуйста, введите правильный email')]"));
			inputField(By.xpath("//input[@type='email'][@name='user_email']"), "test@test.qa");
			assertElements(By.xpath("//label[contains(text(),'Поле, обязательное для заполнения')]"));
			inputField(By.xpath("//textarea[@name='user_message'][@id='inputMessage']"), "b9pv1j2yzm1jsqritazk");
			findElementClick(SendMessage);
            timeout();
			assertText("Ошибка валидации. Пожалуйста, проверьте правильность заполнения полей формы и отправьте еще раз.");
	}
	 
	 
	 
		@Test
		public void goToMainPage(){
			findElementClick(contactUS);
			findElementClick(goToMainPage);
	}
	 
	 
	 
}
