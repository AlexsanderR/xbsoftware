package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class PortfolioRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	
	By linkPortfolio = By.xpath("//*[@id='menu-item-634']/a");
	
	@Test 
	public void linkPortfolio(){
		findElementClick(linkPortfolio);
		assertText("Обзор проектов");
	}
	
	
	@Test
	public void linksInProjectsOverview(){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[@href='#business']")); 
		assertURL("http://xbsoftware.ru/obzor-projektov/#business");
		findElementClick(By.xpath("//a[@href='#marketing']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#marketing");
		findElementClick(By.xpath("//a[@href='#financial']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#financial");
		findElementClick(By.xpath("//a[@href='#information']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#information");
		findElementClick(By.xpath("//a[@href='#logistics']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#logistics");
		findElementClick(By.xpath("//a[@href='#education']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#education");
		findElementClick(By.xpath("//a[@href='#customer']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#customer");
		findElementClick(By.xpath("//a[@href='#e-commerce']"));
		assertURL("http://xbsoftware.ru/obzor-projektov/#e-commerce");
	}
	
	
	@Test
	public void linkCaseStudies (){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[@href='/keisy/']"));
		assertText("Кейсы");
	}
	
	
	@Test 
	public void linkSoftwareDevelopmentCompany(){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[contains(text(),'компания-разработчик программного обеспечения')]"));
		assertText("Наши услуги");
		}
	
	
	@Test
	public void requestAQuote (){
		
	findElementClick(linkPortfolio);
	findElementClick(By.className("to-request-btn"));
	assertText("Свяжитесь с нами");
	}

    @Test
    public void arrowUp(){
        findElementClick(linkPortfolio);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress (){
        findElementClick(linkPortfolio);
        assertElements(By.linkText("info@xbsoftware.com"));
    }

    @Test
    public void goHomePage(){
        findElementClick(linkPortfolio);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }
	
	@Test
	public void linkProjectOverview(){
		findElementClick(linkPortfolio);
		findElementClick(By.xpath("//a[@href='http://xbsoftware.ru/obzor-projektov/']"));
		assertText("Обзор проектов");
	}

}
