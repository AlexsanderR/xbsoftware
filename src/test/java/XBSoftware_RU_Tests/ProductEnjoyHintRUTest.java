package XBSoftware_RU_Tests;

import XBSoftware_Basic.SpecMethodAndVariablesRU;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class ProductEnjoyHintRUTest extends SpecMethodAndVariablesRU{

    By linkProducts = By.xpath("//*[@id='menu-item-516']/a");
    By linkEnjoyHint = By.xpath("//*[@id='menu-item-1307']/a");



    @Test
    public void linkProductsEnjoyHint(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        assertText("Интерактивные подсказки для Вашего веб-сайта или приложения");
    }

    @Test
    public void goHomePage (){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        assertElements(By.linkText("info@xbsoftware.com"));
    }


    @Test
    public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
        assertText("Наши услуги");
    }

    @Test
    public void buttonGetYourFreeEnjoyHint(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.linkText("Скачать EnjoyHint БЕСПЛАТНО"));

    }

    @Test
    public void buttonViewDemo(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-1307')[0].parentNode.style.display = 'block'");
        findElementClick(linkEnjoyHint);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[@title='Демо']"));
        goToURL(currentWindows, "http://xbsoftware.com/demos/enjoyhint/");
        //assertText("Greg Watkinson (supervisor)");
    }
}
