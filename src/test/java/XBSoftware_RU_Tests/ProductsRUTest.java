package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	
	By linkProducts = By.xpath("//*[@id='menu-item-516']/a");

@Test 
public void linkProducts(){
	findElementClick(linkProducts);
	assertText("Наши продукты");
}
    @Test
    public void linkGoHomePage(){
        findElementClick(linkProducts);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }

    @Test
    public void arrowUp(){
        findElementClick(linkProducts);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress(){
        findElementClick(linkProducts);
        assertElements(By.linkText("info@xbsoftware.com"));
    }


@Test
public void technologies(){
	findElementClick(linkProducts);
	findElementClick(By.cssSelector("body > div > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(1) > span")); //click inactive css3 in main menu
	assertElements(By.cssSelector("body > div > section.section-products > div > div > div.col-sm-7.tech_post > div > a.active > span")); //assert active css3 in webix
	findElementClick(By.cssSelector("body > div > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span")); //click active css3 in main menu
	assertElements(By.cssSelector("body > div > section.section-products > div > div:nth-child(5) > div.col-sm-7.tech_post > div > a:nth-child(1) > span")); //assert inactive css3 in webix
	
	findElementClick(By.cssSelector("body > div > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(3) > span"));// click inactive javascript in main menu
	assertElements(By.cssSelector("body > div > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a.active > span")); //assert active in staff
	findElementClick(By.cssSelector("body > div > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span")); //click active in main menu
	assertElements(By.cssSelector("body > div > section.section-products > div > div:nth-child(1) > div.col-sm-7.tech_post > div > a:nth-child(2) > span")); //assert inactive in staff
	
	findElementClick(By.cssSelector("body > div > section.section_technology_tags > div > div.post_tags.portfolio_tags > a:nth-child(6) > span")); //click inactive scrum in main menu
	assertElements(By.cssSelector("body > div > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a.active > span"));//assert active in xbtrack
	findElementClick(By.cssSelector("body > div > section.section_technology_tags > div > div.post_tags.portfolio_tags > a.active > span"));//click active in main menu
	assertElements(By.cssSelector("body > div > section.section-products > div > div:nth-child(3) > div.col-sm-7.tech_post > div > a:nth-child(3) > span"));//assert inactive in xbtrack

}

@Test 
public void buttonViewDemoStaffManager(){
	findElementClick(linkProducts);
	String currentWindows = driver.getWindowHandle();
	findElementClick(By.xpath("//a[@href='/demos/staff-manager/ ']"));
	goToURL(currentWindows, "http://xbsoftware.ru/demos/staff-manager/");
}

@Test 
public void buttonViewDemoBtrack(){
	findElementClick(linkProducts);
	String currentWindows = driver.getWindowHandle();
	findElementClick(By.xpath("//a[@href='/demos/btrack/ ']"));
	goToURL(currentWindows, "http://xbsoftware.ru/demos/btrack/login.php");
}

@Test 
public void buttonViewDemoWebix(){
	findElementClick(linkProducts);
	String currentWindows = driver.getWindowHandle(); 
	findElementClick(By.xpath("//a[@href='http://webix.com/ru/demos/ ']"));
	goToURL(currentWindows, "http://webix.com/ru/demos/");
}
    @Test
    public void buttonViewDemoEnjoyHint(){
        findElementClick(linkProducts);
        String currentWindows = driver.getWindowHandle();
        findElementClick(By.xpath("//a[@href='http://xbsoftware.com/demos/enjoyhint/']"));
        goToURL(currentWindows, "http://xbsoftware.com/demos/enjoyhint/");
    }



@Test
public void requestAQuote(){
	findElementClick(linkProducts);
	waitForElement(linkProducts);
	findElementClick(By.xpath("/html/body/div/section[3]/div/div[1]/div[2]/a[1]"));
	assertText("Свяжитесь с нами");
	findElementClick(linkProducts);
	waitForElement(linkProducts);
	findElementClick(By.xpath("/html/body/div/section[3]/div/div[2]/div[2]/a[1]"));
	assertText("Свяжитесь с нами");
	findElementClick(linkProducts);
	waitForElement(linkProducts);
	findElementClick(By.xpath("/html/body/div/section[3]/div/div[3]/div[2]/a[1]"));
	assertText("Свяжитесь с нами");
			
}
	
	

}
