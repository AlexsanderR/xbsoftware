package XBSoftware_RU_Tests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class AboutUsRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	                                     
	By linkAboutUs = By.xpath("//*[@id='menu-item-232']/a");
	By Otpravitq = By.xpath("//input[@type='submit'][@value='Отправить']");
	By inputName = By.id("inputName");
	By inputEmail = By.id("inputEmail");
	By inputPhone = By.id("inputPhone");
	By goToMainPage = By.xpath("//a[@href='http://xbsoftware.ru ']");

    @Test
    public void linkSoftwareDevelopmentCompany(){
        findElementClick(linkAboutUs);
        findElementClick(By.linkText("аутсорсинговая компания-разработчик"));
        assertText("Наши услуги");
    }

	@Test
	public void sendMessage(){
		findElementClick(linkAboutUs);
		findElementClick(Otpravitq);
		assertElements(By.xpath("//label[@for='inputName'][contains(text(), 'Поле, обязательное для заполнения')]"));
		inputField(inputName, "test");
		inputField(inputEmail, "sddasd");//sendIncorrectEmail
		assertElements(By.xpath("//label[@for='inputEmail'][contains(text(), 'Пожалуйста, введите правильный email')]"));
		inputField(inputEmail, "test@test.test");//inputEmailText
		inputField(inputPhone, "dsdf");//sendIncorrectPhone
		assertElements(By.xpath("//label[@for='inputPhone'][contains(text(), 'Пожалуйста, введите правильный номер телефона')]"));//assertIncorrectPhone
		inputField(inputPhone, "8136661313");//inputPhoneNumber
		assertElements(By.xpath("//label[@for='inputMessage'][contains(text(), 'Поле, обязательное для заполнения')]"));
		inputField(By.id("inputMessage"), "b9pv1j2yzm1jsqritazk");//inputMessage
		findElementClick(Otpravitq);
		timeout();
		assertText("Ошибка валидации. Пожалуйста, проверьте правильность заполнения полей формы и отправьте еще раз.");
	}
	
	@Test
	public void up(){
		findElementClick(linkAboutUs);
		findElementClick(By.id("arrow-up"));
				
	}
	
	@Test
	public void goToMainPage(){
		findElementClick(linkAboutUs);
		findElementClick(goToMainPage);
		assertText("Наши услуги");
		
	}
}
