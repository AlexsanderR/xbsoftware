package XBSoftware_RU_Tests;

import XBSoftware_Basic.SpecMethodAndVariablesRU;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class SearchRUTest extends SpecMethodAndVariablesRU {

    By search = By.className("search-icon");
    By inputSearch = By.xpath("//input[@id='search']");
    By searchElement = By.xpath("//input[@class='pull-right']");


    @Test
    public void hideSearch(){
        findElementClick(search);
        findElementClick(By.className("hide-search-form"));
    }

    @Test
    public void searchEmptyInformation(){
        findElementClick(search);
        findElementClick(searchElement);
        assertText("результатов");

    }

    @Test
    public void searchWithError(){
        findElementClick(search);
        inputField(inputSearch, "testtesttest");
        findElementClick(searchElement);
        waitForElement(By.cssSelector(".title-404"));
        assertText("Извините, мы не нашли ничего по заданным критериям поиска. ");
        //findElementClick(search);

    }

    @Test void search(){
        findElementClick(search);
        inputField(inputSearch, "компания");
        findElementClick(searchElement);
        timeout();
        assertText("результатов");
    }

    @Test
    public void goToNextPage(){
        findElementClick(search);
        inputField(inputSearch, " ");
        findElementClick(searchElement);
        waitForElement(By.linkText("3"));
        findElementClick(By.linkText("3"));
    }

    @Test
    public void goToNextPost(){
        findElementClick(search);
        inputField(inputSearch, " ");
        findElementClick(searchElement);
        waitForElement(By.linkText("Next post"));
        findElementClick(By.linkText("Next post"));
    }

    @Test
    public void homeLink(){
        findElementClick(search);
        inputField(inputSearch, "testtesttest");
        findElementClick(searchElement);
        waitForElement(By.cssSelector(".home-link"));
        findElementClick(By.cssSelector(".home-link"));
    }
}
