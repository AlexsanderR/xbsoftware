package XBSoftware_RU_Tests;

import XBSoftware_Tests.MainPageTest;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class BlogRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{

	 By blog = By.xpath("//*[@id='menu-item-328']/a");
	 By buttonUp = By.id("arrow-up");
	 By info = By.linkText("info@xbsoftware.com");
	 By goToMainPage = By.xpath("//a[@href='http://xbsoftware.ru ']");
	 static Logger LOG = Logger.getLogger(MainPageTest.class);
	 
	 @Test
		public void TagsAllPosts(){
		 	findElementClick(blog);
		 	findElementClick(By.className("post_title"));
			findElementClick(By.xpath("//span[@class='truncate'][contains(text(), 'Все статьи')]"));
			assertText("Подпишитесь на видеодайджест и статьи для веб-разработчиков");
			
			
		}
		
			
		@Test
		public void RecentPost (){
			findElementClick(blog);
            ((JavascriptExecutor)driver).executeScript("document.querySelector(\".recent-posts-links li:nth-child(1) a\").click()");
			findElementClick(By.linkText("Блог"));
			assertText("Другие статьи");
		}
		
		@Test
		public void ReadMore (){
			findElementClick(blog);
			findElementClick(By.linkText("Подробнее"));
			findElementClick(By.linkText("Блог"));
			assertText("Другие статьи");
		}
		
		@Test
		public void emailAddress(){
			findElementClick(blog);
		assertElements(info);
		}
		
		@Test
		public void goHomePage(){
			findElementClick(blog);
			findElementClick(goToMainPage);
			assertText("Наши услуги");
		}
		
		@Test
		public void linkXBSoftware (){
			findElementClick(blog);
			findElementClick(By.xpath("//a[@href='http://xbsoftware.ru/'][contains(text(), 'XB Sofware')]"));
			assertText("Наши услуги");
		}
		

		@Test
		public void linkOnFirstPostOnPage (){
			findElementClick(blog);
			findElementClick(By.className("post_title"));
			findElementClick(By.linkText("Блог"));
		}
		
		@Test
		public void linkLeaveAComment(){
			findElementClick(blog);
			findElementClick(By.xpath("//div[@class='comments-count']")); 
			assertText("Комментарии");
		}
		
		
		
		@Test
		public void pagination(){
			findElementClick(blog);
			findElementClick(By.linkText("2")); 
			findElementClick(By.xpath("//a[@href='http://xbsoftware.ru/blog/']"));
		}
		
		@Test
		public void arrowUp(){
			findElementClick(blog);
			findElementClick(buttonUp);
		}


    By singUp = By.cssSelector(".subscribe-block-btn input");
    By name = By.cssSelector("input[name='subscription_name']");
    By email = By.cssSelector("input[name='subscription_email']");


    @Test
		public void singUp() {
        findElementClick(blog);
        findElementClick(singUp);
         assertElements(By.cssSelector("label:contains('Пожалуйста, введите имя')"));
        inputField(name, "test");
        assertElements(By.cssSelector("label:contains('Пожалуйста, введите email')"));
        inputField(email, "test");
        assertElements(By.cssSelector("label:contains('Пожалуйста, введите email')"));
        inputField(email, "тест@тест.тест");
        findElementClick(singUp);
        timeout();
        assertElements(By.cssSelector("label:contains('Пожалуйста, введите email')"));
        inputField(email, "test@test.test");
        findElementClick(singUp);
        timeout();
        assertElements(By.xpath("//*[.='Вы уже подписаны']"));
        findElementClick(blog);
        inputField(name, "test");
        String emailAddress = "test" + randomText(5) + "@test.test";
        inputField(email, emailAddress);
        findElementClick(singUp);
        timeout();
        assertElements(By.xpath("//*[.='Благодарим за подписку']"));

    }

        @Test
        public void privacyPolicy(){
            findElementClick(blog);
            findElementClick(By.cssSelector(".row-subscr-form a"));
            assertElements(By.cssSelector("span:contains('Политика Конфиденциальности')"));
        }

    @Test
    public void videodaidjest(){
        findElementClick(blog);
         findElementClick(By.cssSelector(".v-padding30:nth-child(2) .category_tags a:nth-child(13)"));
        assertText("Видеодайждест №4. Все презентации разработчиков с What The Hack 2014");
    }

    @Test
    public void privacyPolicyVideodaijest(){
        findElementClick(blog);
        findElementClick(By.cssSelector(".v-padding30:nth-child(2) .category_tags a:nth-child(13)"));
        findElementClick(By.cssSelector(".post_title"));
        assertText("Подпишитесь на нашу рассылку и получайте");
        findElementClick(By.cssSelector(".subscribe-wrapper .row-subscr-form a"));
        assertElements(By.cssSelector("span:contains('Политика Конфиденциальности')"));
    }


    By singUpVideo = By.cssSelector(".subscribe-block-btn input");
    By nameVideo = By.xpath("//input[@name='subscription_name']");
    By emailVideo = By.xpath("//input[@name='subscription_email']");

    @Test
    public void singUpVideodaijest() {
        findElementClick(blog);
        findElementClick(By.cssSelector(".v-padding30:nth-child(2) .category_tags a:nth-child(13)"));
        findElementClick(By.cssSelector(".post_title"));
        timeout();
        findElementClick(singUpVideo);
        assertElements(By.cssSelector("label:contains('Пожалуйста, введите имя')"));
        inputField(nameVideo, "test");
        assertElements(By.cssSelector("label:contains('Пожалуйста, введите email')"));
        inputField(emailVideo, "test");
        assertElements(By.cssSelector("label:contains('')"));
        inputField(emailVideo, "тест@тест.тест");
        findElementClick(singUpVideo);
        timeout();
        assertElements(By.cssSelector("label:contains('Пожалуйста, введите email')"));
        inputField(emailVideo, "test@test.test");
        findElementClick(singUpVideo);
        timeout();
        assertElements(By.xpath("//*[.='Вы уже подписаны']"));
        findElementClick(blog);
        findElementClick(By.cssSelector(".v-padding30:nth-child(2) .category_tags a:nth-child(13)"));
        findElementClick(By.cssSelector(".post_title"));
        inputField(nameVideo, "test");
        String emailAddress = "test" + randomText(5) + "@test.test";
        inputField(emailVideo, emailAddress);
        findElementClick(singUpVideo);
        timeout();
        assertElements(By.xpath("//*[.='Благодарим за подписку']"));
    }



}
