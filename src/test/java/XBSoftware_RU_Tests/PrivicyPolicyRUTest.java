package XBSoftware_RU_Tests;

import XBSoftware_Basic.SpecMethodAndVariablesRU;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class PrivicyPolicyRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{


    @Test
    public void linkXBsoftware(){
        driver.get("http://xbsoftware.ru/politika-konfidentsialnosti/");
        findElementClick(By.cssSelector("a[href='http://xbsoftware.ru/']"));
        assertElements(By.cssSelector("span:contains('Наши услуги')"));
    }

    @Test
    public void contactUs(){
        driver.get("http://xbsoftware.ru/politika-konfidentsialnosti/");
        findElementClick(By.linkText("напишите нам"));
        assertText("Контакты");
    }

    @Test
    public void informationTest(){
        driver.get("http://xbsoftware.ru/politika-konfidentsialnosti/");
        assertElements(By.linkText("info@xbsoftware.com"));
    }

    @Test
    public void upTest(){
        driver.get("http://xbsoftware.ru/politika-konfidentsialnosti/");
        findElementClick(By.id("arrow-up"));
    }
}
