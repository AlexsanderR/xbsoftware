package XBSoftware_RU_Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class ProductsWebixRUTest extends XBSoftware_Basic.SpecMethodAndVariablesRU{
	
	By linkProducts = By.xpath("//*[@id='menu-item-516']/a");
	By linkWebix = By.xpath("//*[@id='menu-item-749']/a");
	By arrowUp  = By.id("arrow-up");
	
	
	@Test 
	public void linkProductsWebix(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
		findElementClick(linkWebix);
		waitForElement(arrowUp);
		assertText("JavaScript библиотека UI виджетов");
	}

    @Test
    public void linkGoHomePage(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebix);
        findElementClick(By.cssSelector("div[class='logo'] a"));
        assertText("Наши услуги");
    }

    @Test
    public void arrowUp(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebix);
        findElementClick(By.id("arrow-up"));
    }

    @Test
    public void emailAddress(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebix);
        assertElements(By.linkText("info@xbsoftware.com"));
    }
	
	@Test
	public void linkXBSoftware(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebix);
		findElementClick(By.xpath("//a[contains(text(),'XB Software')]"));
		waitForElement(arrowUp);
		assertText("Наши услуги");
	}
	
	@Test
	public void buttonReadMore(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
		findElementClick(linkWebix);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@class='btn btn-send']"));
		goToURL(currentWindows, "http://webix.com/ru/");
		assertText("Богатое разнообразие виджетов");
	}
	
	@Test
	public void buttonViewDemo(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
		findElementClick(linkWebix);
		String currentWindows = driver.getWindowHandle();
		findElementClick(By.xpath("//a[@title='Демо']"));
		goToURL(currentWindows, "http://webix.com/ru/demos/");
		
	}

    @Test
    public void otherProducts(){
        ((JavascriptExecutor)driver).executeScript("jQuery('#menu-item-749')[0].parentNode.style.display = 'block'");
        findElementClick(linkWebix);
        assertText("Другие продукты:");
        findElementClick(By.xpath("//*[@class='caption-product-link']"));
    }
}
